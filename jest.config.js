module.exports = {
    preset: 'jest-expo',
    verbose: true,
    clearMocks: true,
    testMatch: [ 
        "**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[jt]s?(x)" 
    ],
    moduleNameMapper: {

    },
    transform: {
        '\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
    },
    transformIgnorePatterns: [


    ]
};