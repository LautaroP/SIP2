import React, { Component } from 'react';
import { Button, StyleSheet, Text, View, TextInput, ScrollView, Image, ImageBackground, SafeAreaView, ActivityIndicator, StatusBar } from 'react-native';
import { createBottomTabNavigator, createSwitchNavigator, createAppContainer, TabNavigator, createStackNavigator, DrawerNavigator, createDrawerNavigator, DrawerItems } from 'react-navigation';
import NAVIGATION from './assets/constants'
import crearPost from './src/Views/crearPost';
import mainMenu from './src/Views/mainMenu';
import FiltrarIniciativas from './src/Views/filtrarIniciativas/index';
import ResultadoFiltro from './src/Views/filtrarIniciativas/resultadoFiltro';
import addFiles from './src/Views/addFiles';
import MenuHeader from './components/menuHeader';
import DonateScreen from './src/Views/donar/index';
import Iniciativa from './src/Views/iniciativa/index';
import Participacion from './src/Views/iniciativa/Participacion';
import Perfil from './src/Views/perfil/index';
import MisColaboraciones from './src/Views/perfil/MisColaboraciones';
import LoginScreen from './src/Views/login/index';
import signinScreen from './src/Views/signin/index';
import navigationService from './src/navigationService';
import { connect } from 'react-redux';
import EmptyComponent from './src/Views/EmptyComponent/index'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { colors } from './src/index.styles';
import { FontAwesome } from '@expo/vector-icons';
import PerfilDrawer from './src/Views/PerfilDrawer';
import ResultadoFiltroPalabra from './src/Views/filtrarIniciativas/resultadoPalabra'
import { Colors } from 'react-native-paper';
import Logo from './src/icons/hu.png'
class Hidden extends React.Component {
  render() {
    return null;
  }
}

const ROOTSTACK = () => {
  return createDrawerNavigator({
    'TABS_STACK': {
      screen: bottomTabStack,
    },
    'SIGN_IN': {
      screen: createStackNavigator({
        SIGN_IN: {
          screen: signinScreen,
          navigationOptions: ({ navigation }) => navigationOptionsMenu(
            {
              title: 'Registrarse',
              navigation: navigation,
              haveBack: true,
            }
          ),
        }
      })
    },
  }, {
    navigationOptions: {
      header: null
    },
    contentComponent: (props) => (
      <View style={{ flex: 1 }}>
        <SafeAreaView forceInset={{ bottom: 'never' }} style={{ flex: 1, alignContent: 'center', justifyContent: 'center', alignItems: "center", alignSelf: "center" }}>
          <LoginScreen navigation={props.navigation} />
        </SafeAreaView>
      </View>
    ),
    unmountInactiveRoutes: true,
    headerMode: 'none',
    initialRouteName: 'TABS_STACK'
  })
}


const bottomTabStack = createMaterialBottomTabNavigator({
  MAIN_MENU: {
    screen: createStackNavigator({
      main: {
        screen: mainMenu,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Help Up!",
          navigation: navigation,
          haveBack: false,
        }),
      },
      INICIATIVA: {
        screen: Iniciativa,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Iniciativa",
          navigation: navigation,
          haveBack: true,
        }),
      },
      ERROR_SCREEN: {
        screen: EmptyComponent,
        navigationOptions: ({ navigation }) => navigationOptionsMenu(
          {
            title: 'ERROR',
            navigation: navigation,
            haveBack: false,
          }, true
        ),
        params: {
          titleText: 'Algo salió mal',
          descText: 'Vuelva a intentarlo mas tarde',
        }
      },
    }),
    navigationOptions: {
      tabBarLabel: 'Help Up!',
      tabBarIcon: ({ tintColor }) => tabBarIcon(tintColor, 'home')
    }
  },
  CREATE_POST: {
    screen: createStackNavigator({
      'CREATE_POST': {
        screen: crearPost,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Crear Iniciativa",
          navigation: navigation,
          haveBack: false,
        }),
      }, 'ADD_FILES': {
        screen: addFiles,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Crear Iniciativa",
          navigation: navigation,
          haveBack: true,
        }),
      },
    }
    ),
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => tabBarIcon(tintColor, 'plus'),
      tabBarLabel: 'Crear Iniciativa'
    }
  },
  FILTRAR: {
    screen: createStackNavigator({
      'FILTRAR': {
        screen: FiltrarIniciativas,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Filtrar Iniciativas",
          navigation: navigation,
          haveBack: false,
        }),
      }, 'FILTER_RESTULTS': {
        screen: ResultadoFiltro,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Filtrar Iniciativas",
          navigation: navigation,
          haveBack: true,
        }),
      },
      'FILTER_RESTULTS_PALABRA': {
        screen: ResultadoFiltroPalabra,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Filtrar Iniciativas",
          navigation: navigation,
          haveBack: true,
        }),
      },
      INICIATIVA: {
        screen: Iniciativa,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Iniciativa",
          navigation: navigation,
          haveBack: true,
        }),
      },
      PARTICIPACION: {
        screen: Participacion,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Participar",
          navigation: navigation,
          haveBack: true,
        }),
      },
      'DONATE': {
        screen: DonateScreen,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Donar",
          navigation: navigation,
          haveBack: true,
        }),
      },
    }
    ),
    navigationOptions: {
      tabBarLabel: 'Buscar',
      tabBarIcon: ({ tintColor }) => tabBarIcon('tintColor', 'search')
    }
  },
  PERFIL: {
    screen: createStackNavigator({
      'PERFIL': {
        screen: Perfil,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Perfil",
          navigation: navigation,
          haveBack: false,
        }),
      },
      'MISCOLABORACIONES': {
        screen: MisColaboraciones,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Mis Colaboraciones",
          navigation: navigation,
          haveBack: true,
        }),
      },
      INICIATIVA: {
        screen: Iniciativa,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Iniciativa",
          navigation: navigation,
          haveBack: true,
        }),
      },
      PARTICIPACION: {
        screen: Participacion,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Participar",
          navigation: navigation,
          haveBack: true,
        }),
      },
      'DONATE': {
        screen: DonateScreen,
        navigationOptions: ({ navigation }) => navigationOptionsMenu({
          title: "Donar",
          navigation: navigation,
          haveBack: true,
        }),
      },
    }),
    navigationOptions: {
      tabBarLabel: 'Perfil',
      tabBarIcon: ({ tintColor }) => tabBarIcon(tintColor, 'user')
    }
  }
}, {
  shifting: false,
  activeColor: '#16B9D9',
  inactiveColor: '#16B9D9',
  barStyle: { backgroundColor: 'white', borderTopColor: Colors.grey100, borderTopWidth:1},
}
)
function createRootNavigator() {
  return createAppContainer(ROOTSTACK());
}

class App extends Component {

  constructor(props) {
    super(props)
  }


  componentDidMount() {

    StatusBar.setHidden(true);
    console.disableYellowBox = true;
  }

  render() {
    return this._renderAppRoot();
  }

  _renderAppRoot() {
    const CreateRoot = createRootNavigator();
    return (<CreateRoot ref={navigatorRef => {
      navigationService.setTopLevelNavigator(navigatorRef);
    }} />)

  }

  _renderSplash() {
    return (
      <SafeAreaView style={[appStyles.container, appStyles.droidSafeArea]}>
        <ActivityIndicator size='large' />
      </SafeAreaView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
};

const mapDispatchToProps = (dispatch) => {
  return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(App)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
    backgroundColor: '#f8f8f8'

  },
  titulo: {
    padding: 20,
    backgroundColor: 'lightgrey',
    color: '#16B9D9',
    fontSize: 35,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: 'rgba(52, 52, 52, 0.01)',

  },

  scroll: {
    flex: 1,
    padding: 20,
    backgroundColor: 'red',
  },
  buttonContainer: {
    backgroundColor: 'red',
    fontWeight: 'bold',
    borderRadius: 10,
    padding: 10,
    marginTop: 8,
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 10,
    shadowOpacity: 0.25
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    padding: 20,
    justifyContent: 'center',
  }
})
const resizeMode = 'center';
const tabBarIcon = (tintColor, iconName) => (
  <View>
    <FontAwesome style={[{ color: '#16B9D9'}] } size={25} name={iconName}  />
  </View>)

export const navigationOptionsMenu = ({ title, navigation, haveBack, style }, hidden) => {
  const header = (<MenuHeader
    navigation={navigation}
    title={title}
    haveBack={haveBack}
    style={style}
    logo={Logo}
  />)
  return (hidden ? {
    // label: <Hidden />,
    header: header,
  } : {
      header: header,
    })
};