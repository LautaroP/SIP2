import React from 'react';
class SearchApi extends React.Component {

    async search(searchPhrase) {                    
        let finalUrl = `${'https://still-beach-77233.herokuapp.com/rutes/getIniciativasByWord/'}${searchPhrase}` 
        try {
            let response = await fetch (finalUrl);
            const data = await response.json();
            return data
        } catch (err){
            console.log(err)
        }
    }
}

export default new SearchApi();
