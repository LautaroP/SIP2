import navigationService from "../navigationService"

export default secureFetchMessage = (url, body) => {
    return fetch(url, body).then((Response) => {
        console.log(Response.status, Response.ok, url)
        if (Response.ok)
            return Promise.all([Response, Response.text()]).catch(e => {
                console.log('=======text PARSE ERROR=========')
                console.error(e)
                navigationService.navigateToError()
                return Promise.all([])
            })
        else {
            console.log('=======BAD STATUS CODE=========', Response.status)
            navigationService.navigateToError()
            Response.text().then((val) => console.error(val)).catch()
            return Promise.all([])
        }
    }).catch(e => {
        console.log('=======FETCH ERROR========')
        console.error(e)
        navigationService.navigateToError()
        return Promise.all([])
    })
}