import { DOMINION_URL } from "../../assets/constants";
import secureFetch from "./secureFetch";

const POST_LOGIN_URL = DOMINION_URL + 'login';
const POST_SIGNIN_URL = DOMINION_URL + 'insertUsuario';

export const login = (usuario, password) => {
    const requestBody = {
        mail: usuario,
        password: password,
    }
    return fetch(POST_LOGIN_URL, {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    }).then((Response) => {
        console.log(Response.status, Response.ok, POST_LOGIN_URL)
        if (Response.ok)
            return Promise.all([Response, Response.json()]).catch(e => {
                console.error(e)
                return Promise.all([undefined, undefined])
            })
        else {
            return Promise.all([undefined, undefined])
        }
    })
}
export const signin = (nombre, apellido, email, pass, telefono) => {
    const requestBody = {
        nombre: nombre,
        apellido: apellido,
        email: email,
        password: pass,
        telefono: telefono,
        fechaNacimiento: "12-12-1900"
    }
    return fetch(POST_SIGNIN_URL, {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    }).then((Response) => {
        console.log(Response.status, Response.ok, POST_SIGNIN_URL)
        if (Response.ok)
            return Promise.all([Response, Response.text()]).catch(e => {
                console.error(e)
                return Promise.all([undefined, undefined])
            })
        else {
            if (Response.status == 400)
                return Promise.all([undefined, 'El usuario ya existe'])
            return Promise.all([undefined, undefined])
        }
    })
}