import secureFetch from "./secureFetch"
import secureFetchMessage from "./secureFetchMessage"

export const createPost = (titulo, geoLocalizacion, objetivoPersonas, objetivoMonto, fechaLimite, categoria, descripcion, email) => {
    const requestBody = {
        titulo: titulo,
        geoLocalizacion: geoLocalizacion,
        fechaLimite: fechaLimite,
        categoria: categoria,
        descripcion: descripcion,
        impulsor: email,
        objetivoPersonaEsperado: objetivoPersonas,
        objetivoEconomicoEsperado: objetivoMonto,
    }
    console.log(requestBody)
    return secureFetchMessage('https://still-beach-77233.herokuapp.com/rutes/insertIniciativa', {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    })
}