import navigationService from "../navigationService"

export default secureFetch = (url, body) => {
    return fetch(url, body).then((Response) => {
        console.log(Response.status, Response.ok, url)
        if (Response.ok)
            return Promise.all([Response, Response.json()]).catch(e => {
                console.log('=======JSON PARSE ERROR=========')
                console.error(e)
                navigationService.navigateToError()
                return Promise.all([])
            })
        else {
            console.log('=======BAD STATUS CODE=========', Response.status)
            navigationService.navigateToError()
            Response.text().then((val) => console.error(val)).catch()
            return Promise.all([])
        }
    }).catch(e => {
        console.log('=======FETCH ERROR========')
        console.error(e)
        navigationService.navigateToError()
        return Promise.all([])
    })
}