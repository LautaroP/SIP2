import { Alert } from "react-native"
import secureFetch from "./secureFetch"

export const donarIniciativa = (amount, iniciativa,user, callback) => {
    const requestBody = {
        mail: user.email,
        idIniciativa: iniciativa._id,
        donacion: amount,
    }
    console.log('donaaaaaaaaaaaaaaaaaaar', requestBody, iniciativa)
    return fetch('https://still-beach-77233.herokuapp.com/rutes/realizarDonacion', {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    }).then((response) => response.text()).then(json => callback(json)).catch(e => {
        console.error(e)
        callback(false)
    })
}

export const inscribirseEnIniciativa = (iniciativa, user, callback) => {
    console.log('usrApi',user);
    const requestBody = {
        mail: user.email,
        idIniciativa: iniciativa._id,
    }
    console.log('colaborar', requestBody, iniciativa)
    return fetch('https://still-beach-77233.herokuapp.com/rutes/inscribirseEnActividad', {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    }).then((response) => callback(response)).catch(e => {
        console.error(e)
        callback(false)
    })
}


export const getComentarios = (idIniciativa) => {
    return fetch('https://still-beach-77233.herokuapp.com/rutes/getCommentsByIniciativa/' + idIniciativa).then((response) => Promise.all([response.json()])).catch(e => {
        console.error(e)
        Alert.alert("Error al obtener los comentarios")
    })
}

export const postComentario = (comentario) => {
    const requestBody = {
        mail: comentario.mail,
        idIniciativa: comentario.idIniciativa,
        comment: comentario.comment,
    }
    return secureFetch('https://still-beach-77233.herokuapp.com/rutes/insertComentario', {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    }).then(([response, json]) => Promise.all([response.status == 200 || response.status == 204])).catch(e => Promise.all([false]))
} 

export const getImpulsor = (impulsorEmail) => {
    const requestBody = {
        email: impulsorEmail,
    }
    return secureFetch('https://still-beach-77233.herokuapp.com/rutes/getUsuarioByMail', {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
            'Content-Type': 'application/json'
        }),
    })
} 