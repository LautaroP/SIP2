import React from 'react';
class NewsApi extends React.Component {

    async getNews(searchPhrase) {                    
        let finalUrl = 'https://still-beach-77233.herokuapp.com/rutes/getNews/'
        try {
            let response = await fetch (finalUrl);
            const data = await response.json();
            return data
        } catch (err){
            console.log(err)
        }
    }
}

export default new NewsApi();