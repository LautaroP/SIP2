import {
    NavigationActions,
} from '@react-navigation/core';
import { NAVIGATION } from '../assets/constants';
let _navigator;

function setTopLevelNavigator(navigatorRef) {
    _navigator = navigatorRef;
}

function navigate(navParams) {
    _navigator.dispatch(
        NavigationActions.navigate(navParams)
    );
}

function getCurrentRoute() {
    let route = _navigator.state.nav
    while (route.routes) {
        route = route.routes[route.index]
    }
    return route.routeName
}

function navigateToError() {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName: NAVIGATION.TABS_STACK, type: 'Navigation/NAVIGATE', action: {
                type: 'Navigation/NAVIGATE',
                routeName: NAVIGATION.ERROR_SCREEN,
                action: {
                    type: 'Navigation/NAVIGATE',
                    routeName: NAVIGATION.ERROR_SCREEN,
                    params: {
                        title: 'ERROR',
                    }
                }
            }
        })
    );
}

// add other navigation functions that you need and export them

export default {
    _navigator,
    navigate,
    setTopLevelNavigator,
    navigateToError,
    getCurrentRoute,
};