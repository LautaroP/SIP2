import { combineReducers } from 'redux';
import crearPostState from './crearPost/reducer'
import authState from './authState/reducer'
import generalState from './generalState/reducer'
import { LOGOUT } from '../../assets/constants';
const appReducer = combineReducers({
    crearPostState: crearPostState,
    generalState: generalState,
    authState:authState,

});

const rootReducer = (state, action) => {
    // if (action.type === LOGOUT) {
    //     state = undefined
    // }

    return appReducer(state, action)
}

export default rootReducer;