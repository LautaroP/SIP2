import { REDUX } from '../../../assets/constants'

const setLoading = (loading) => {
    return {
        type: REDUX.SET_LOADING,
        payload: {
            loading: loading
        },
    }
}

export default {
    setLoading,
}