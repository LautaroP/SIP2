import { REDUX } from "../../../assets/constants"

const initialState = {
    loading: false,
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case REDUX.SET_LOADING:
            return { ...state, ...payload }
        default:
            return state
    }
}
