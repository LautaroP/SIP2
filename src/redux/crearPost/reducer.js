import { SET_CATEGORIAS, REDUX } from "../../../assets/constants"

const initialState = {
    categorias: [],
    titulo: "",
    descripcion: "",
    fecha: undefined,
    localizacion: "",
    categoria: " ",
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case SET_CATEGORIAS:
            return { ...state, ...payload }
        case REDUX.SET_PASO1_INICIATIVA:
            return { ...state, ...payload }

        default:
            return state
    }
}
