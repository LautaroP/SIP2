import { REDUX } from "../../../assets/constants"

const setPaso1 = (titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto) => {
    return {
        type: REDUX.SET_PASO1_INICIATIVA,
        payload: {
            titulo: titulo,
            descripcion: descripcion,
            fecha: fecha,
            localizacion: localizacion,
            categoria: categoria,
            objetivoPersonas: objetivoPersonas,
            objetivoMonto: objetivoMonto,
        }
    }
}

const attemptSetPaso1 = (titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto, callback) => {
    console.log('=====================',titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto)
    return (dispatch) => {
        dispatch(setPaso1(titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto))
        callback()
    }
}

export default {
    attemptSetPaso1
}