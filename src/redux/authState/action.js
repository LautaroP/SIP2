import { REDUX, LOGOUT } from '../../../assets/constants'
import { login, signin } from '../../api/clientData'
import GeneralActions from '../generalState/action'
import configureStore from '../../../src/store/configureStore'
import { Alert } from 'react-native'

const setLoginData = (userData, email, pass) => {
    return {
        type: REDUX.LOGIN,
        payload: {
            userData: userData,
            email: email,
            pass: pass
        },
    }
}
const attemptSignin = (nombre, apellido, email, pass, telefono, callback) => {
    return (dispatch) => {
        dispatch(GeneralActions.setLoading(true))

        return signin(nombre, apellido, email, pass, telefono).then(([Response, json]) => {
            dispatch(GeneralActions.setLoading(false))
            callback(!!Response, json)
        })
    }
}
const attemptLogin = (email, pass, callback) => {
    return (dispatch) => {
        dispatch(GeneralActions.setLoading(true))

        return login(email, pass).then(([Response, json]) => {
            dispatch(GeneralActions.setLoading(false))
            if (json) {
                dispatch(setLoginData(json, email, pass))
                callback()
            }
            else
                Alert.alert('Lo Siento...', "Su usuario o contraseña son incorrectos.")
        })
    }
}
const reLogin = () => {
    return (dispatch, getState) => {
        console.log('+++++++++++++++++reloging++++++++++++++', getState().authState.email, getState().authState.pass)
        return login(getState().authState.email, getState().authState.pass).then(([Response, json]) => {
            if (json) {
                dispatch(setLoginData(json, getState().authState.email, getState().authState.pass))
            }
        })
    }
}
const logout = () => configureStore.dispatch({ type: LOGOUT })

export default {
    attemptLogin,
    reLogin,
    logout,
    attemptSignin
}