import { REDUX, LOGOUT } from "../../../assets/constants"
const initialState = {
    userData: undefined,
    pass: undefined,
    email:undefined,
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

        case REDUX.LOGIN:
            return { ...state, ...payload }
        case LOGOUT:
            return initialState
        default:
            return state
    }
}
