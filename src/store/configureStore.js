
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import reducers from '../redux/index'
import storage from 'redux-persist/lib/storage';

const persistConfig = {
    key: 'root',
    storage,
};

var configureStore = () => {
    const middlewares = [thunk,/* logMiddleware */];
    const enhancer = applyMiddleware(...middlewares);
    const persistedReducer = persistReducer(persistConfig, reducers);
    let store = createStore(persistedReducer, enhancer);
    return store
}

const store = configureStore()
export default store