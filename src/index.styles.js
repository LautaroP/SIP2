
import { StyleSheet, Platform, StatusBar } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

const colors = {
    BUTTON_DISABLED: '#C7DDEA',
    BLACK: "#000",
    WHITE: "#FFF",
    APP_BLUE: "#2099FB",
    SILVER: "#BEBEBE",
    TORCH_RED: "#FF0000",
    BUTTON_HOVER_BLUE: "#025190",
    TEXT_BLUE: "#015190",
    TEXTO_PLANO_GREY: "#767676",
    BORDER_GREY_COLOR: '#b5b5b5',
    ITEMS_HEADER: "#a8d9ea",
    APP_GREEN: "#480000",
    APP_BLUE_HOVER: "#295000",
    APP_GREY_HOVER: "#425000",
    TEXTO_PLANO_GREEN: "#3bb000",
    TEXTO_PLANO_GREY_SMALL: "#878787",
    PHONE_ICON_BLUE: "#0093cb",
    MARKER_ICON_BLUE: "#004094",
    APP_LIGHT_GREY_COLOR: '#f7f7f7',
    APP_GREY_COLOR: '#F2F3F4',
    APP_LIGHT_BLUE: '#2699FB',
    PRUSIAN: '#003153'
};

const appStyles = StyleSheet.create({
    SpinnerView: {
        flex: 1,
        width: "95%",
        alignSelf: 'center',
        marginBottom: 20,
    },
    droidSafeArea: {
        flex: 1,
        backgroundColor: colors.APP_BLUE,
        paddingTop: Platform.OS === 'android' ? 25 : 0
    },
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    horizontalContainer: {
        flex: 1,
        flexDirection:'row',
        alignItems: "center",
        justifyContent: "center",
    },

    horizontalForm: {
        flexDirection: 'row',
        justifyContent: "center",
        width: wp("80%"),
    },
    horizontalView: {
        flexDirection: 'row',
    },
    horizontalFullView: {
        width: '100%',
        flexDirection: 'row',
    },
    alignItemsCenter: {
        alignItems: "center",
    },

    /* Formulario */
    form: {
        justifyContent: "center",
        width: wp("90%"),
        //marginBottom: hp('1%')
    },
    scrollContainer: {
        flex: 1,
        width: "100%",
    },
    textoPlanoGris: {
        color: colors.TEXTO_PLANO_GREY,
        textAlign: 'center',
        fontSize: 13,
    },
    textoGrisLink: {
        color: colors.TEXTO_PLANO_GREY,
        textAlign: 'center',
        fontSize: 17,
    },
    textoPlanoGrisTitle: {
        color: colors.BLACK,
        textAlign: 'center',
        fontSize: 22,
        opacity: 0.7
    },
    textoPlanoGrisBig: {
        color: colors.TEXTO_PLANO_GREY,
        textAlign: 'center',
        fontSize: 22,
    },
    textoPlanoGrisSmall: {
        color: colors.TEXTO_PLANO_GREY_SMALL,
        textAlign: 'center',
        fontSize: 12,
    },
    fullWidth: {
        width: '100%'
    },
    textBlue: {
        color: colors.TEXT_BLUE,
        textAlign: 'center',
        fontSize: 16,
    },

    linkPlanoGris: {
        textDecorationLine: 'underline',
        color: colors.TEXTO_PLANO_GREY,
        fontSize: 13,
    },


    captionGris: {
        color: colors.TEXTO_PLANO_GREY,
        fontSize: 13,
    },
    textoPlanoGrisBold: {
        color: colors.TEXTO_PLANO_GREY,
        textAlign: 'center',
        fontSize: 13,
        fontWeight: "bold",
    },
    textoPlanoGrisNoAlignment: {
        color: colors.TEXTO_PLANO_GREY,
        fontSize: 13,
    },
    textoPlanoAzul: {
        color: colors.APP_BLUE,
        textAlign: 'center',
        fontSize: 13
    },
    textoPlanoAzulBig: {
        color: colors.APP_BLUE,
        textAlign: 'center',
        fontSize: 15
    },
    horizontalFormHeader: {
        flexDirection: 'row',
        width: "100%",
        height: "10%",
        marginBottom: 3,
        alignItems: 'center'
    },
    textoPlanoAzulBold: {
        color: colors.APP_BLUE,
        textAlign: 'center',
        fontSize: 13,
        fontWeight: "bold",
    },
    textoPlanoAzulBoldBig: {
        color: colors.APP_BLUE,
        textAlign: 'center',
        fontSize: 15,
        fontWeight: "bold",
    },
    textTitulo: {
        color: colors.APP_LIGHT_BLUE,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: "bold",
        marginTop: 15
    }, textTituloGrande: {
        color: colors.APP_LIGHT_BLUE,
        textAlign: 'center',
        fontSize: 30,
        fontWeight: "bold",
        marginTop: 15
    },
    textTituloGigante: {
        color: colors.APP_LIGHT_BLUE,
        textAlign: 'center',
        fontSize: 60,
        fontWeight: "bold",
        marginTop: 40
    },
    textoHoverAzulBoldBig: {
        color: colors.APP_BLUE_HOVER,
        textAlign: 'center',
        fontSize: 15,
        fontWeight: "bold",
        textDecorationLine: 'underline'
    },
    textoHoverGris: {
        color: colors.APP_GREY_HOVER,
        textAlign: 'center',
        fontSize: 17,
        textDecorationLine: 'underline'
    },
    textoPlanoAzulBoldGiant: {
        color: colors.APP_BLUE,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: "bold",
    },

    textoAlertaRojo: {
        color: colors.TORCH_RED,
        textAlign: 'center',
        fontSize: 13,
        height: hp('3%')
    },

    textoNonCenterVerdeBig: {
        color: colors.TEXTO_PLANO_GREEN,
        fontSize: 14,
    },
    textoVerdeBigHeader: {
        color: colors.TEXTO_PLANO_GREEN,
        fontSize: 15,
        //fontWeight: "bold",
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderBottomColor: colors.BORDER_GREY_COLOR,
    },
    /* Texto alineado y justificado */
    fullJustify: {
        alignContent: 'center',
        alignItems: "center",
        justifyContent: 'center',
        paddingHorizontal: 5,
    },

    /* Flecha Back del Header y Hamburguesa  */
    /*  */itemHeader: {
        paddingHorizontal: 7,
        color: colors.ITEMS_HEADER,
    },

    disabled: {
        opacity: 0.3
    },

    enabled: {
        opacity: 1
    },

    iconHeader: {
        height: 26,
        width: 26,
        alignSelf: "center",
        // marginLeft: "auto",
        // marginRight: "auto",
    },

    textHeader: {
        //justifyContent: 'center',
        color: colors.WHITE,
        //alignContent: 'center',
        fontSize: 17,
        paddingHorizontal: 5
    },

    /* Imagen Fondo */
    imageBackGround: {
        backgroundColor: colors.WHITE,
        width: '100%',
        height: '100%'
    },
    imageBackGroundOpacity: {
        flex: 1,
        backgroundColor: colors.WHITE,
        width: '100%',
        height: '100%',
        opacity: 0.9
    },

    textInput: {
        height: 29,
        width: '100%',
        paddingLeft: 5,
        fontSize: 13,
        //paddingTop: 15,
    },
    textInputRequired: {
        height: 29,
        width: '100%',
        paddingLeft: 5,
        fontSize: 13,
        //paddingTop: 15,
    },
    textInputBorder: {
        marginBottom: 20,
        borderColor: colors.TEXTO_PLANO_GREY,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    marginTop20: {
        marginTop: 20,
    },
    marginTop50: {
        marginTop: 50,
    },
    marginTop10: {
        marginTop: 10,
    },
    marginTop30: {
        marginTop: 30,
    },
    marginBottom5p: {
        marginTop: hp('5%'),
    },
    paddingTop: {
        paddingTop: 12,
    },
    paddingVertical: {
        paddingVertical: 12
    },

    trianguloCombo: {
        height: 38,
        width: '100%',
        borderColor: colors.SILVER,
        borderBottomWidth: StyleSheet.hairlineWidth,
        marginBottom: 20
    },
    buttonBlue: {
        width: wp("90%"),
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.APP_BLUE,
        marginTop: 15,
        marginBottom: 15,
        //paddingVertical: 6,
        borderRadius: 8,
        //paddingBottom: 8,
    },

    textButton: {
        color: colors.WHITE,
        textAlign: "center",
        height: 20,
        fontSize: 15,
        marginBottom: 3
        // paddingHorizontal: 20
    },

    width70: {
        width: '70%',
    },
    height50: {
        height: '50%',
    },

    height50p: {
        height: hp('50%'),
    },
    height80p: {
        height: hp('80%'),
    },
    height75p: {
        height: hp('75%'),
    },

    paddingLeft: {
        paddingLeft: 10
    },
    greenIcon: {
        color: colors.APP_GREEN,
        alignSelf: 'center'
    },
    greyIcon: {
        color: colors.BORDER_GREY_COLOR,
        alignSelf: 'center',
        //borderRadius: 90,
        //height: 20

    },
    markerIcon: {
        color: colors.MARKER_ICON_BLUE,
        alignSelf: 'flex-start',
        marginRight: 5,
        marginTop: 4,
        justifyContent: 'flex-start'
    },
    phoneIcon: {
        color: colors.PHONE_ICON_BLUE,
        marginTop: 6,
        marginRight: 5,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    requiredToken: {
        color: 'red',
        paddingLeft: 5,
        fontSize: 13,
        paddingTop: 7,
    },
    requiredTokenCaption: {
        color: 'red',
        // paddingBottom: 25,
        paddingRight: 5,
        paddingTop: 2,
    },
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: "white",
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    }

});

export { colors, appStyles };