import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, KeyboardAvoidingView, Text, Alert, StyleSheet, Dimensions, ScrollView } from 'react-native'
//import TextInput from '../../../components/textInput/textInput'
//import Button from '../../../components/Button/button'
import { appStyles } from '../../index.styles';
import LoginActions from '../../redux/authState/action'
//import PasswordInput from '../../../components/passwordInput/passwordInput';
import SpinnerView from '../../../components/SpinnerView';
import { NAVIGATION } from '../../../assets/constants';
//import { Button } from 'react-native-paper';
import TextInput from "../../../components/textInput/textInputPaper"
import PasswordInput from "../../../components/passwordInput/passwordPaper"
import NumericInput from "../../../components/numericInput/numericInputPaper"
import Button from "../../../components/Button/buttonPaper"

const { width, height } = Dimensions.get("window")
const mapStateToProps = (state) => {
    return {
    };
}
class SigninScreen extends Component {
    state = {
        email: '',
        nombre: '',
        apellido: '',
        fechaNacimiento: '',
        pass: '',
        telefono: "",
        passConf: '',
    }
    verfySignIn = () => {
        if (this.state.pass != this.state.passConf) {
            Alert.alert('Datos Incorrectos', 'Las contraseñas no coinciden')
            return false
        }
        const { pass, nombre, email, apellido, telefono } = this.state
        if (!(pass && nombre && email && apellido && telefono)) {
            Alert.alert('Datos Incorrectos', 'Complete todos los campos')
            return false
        }
        return true
    }
    trySignin = () => {
        const { nombre, apellido, email, pass, telefono } = this.state
        //console.log(this.state);

        if (this.verfySignIn())
            this.props.attemptSignin(nombre, apellido, email, pass, telefono, (bool, msg) => {
                if (bool) {
                    Alert.alert( "Se ha registrado con éxito, ya puede iniciar sesión")
                    this.props.navigation.navigate(NAVIGATION.TABS_STACK)
                    this.props.navigation.toggleDrawer()
                }
                else
                    Alert.alert("Lo siento!", msg ? msg : "Hubo un error en la creación del usuario.")
            })
    }
    render() {
        return (
            <SpinnerView>
                <ScrollView style={{ flex: 1 }} contentContainerStyle={styles.container}>
                    {
                        //<Text style={appStyles.textTituloGrande}>HELP UP!</Text>
                    }
                    <TextInput
                        label={"Nombre"}
                        style = {styles.textInput}
                        onChangeText={(text) => {
                            this.setState({ nombre: text })
                            //console.log("text" + text);
                            //console.log("nombre: " + this.state.nombre);

                        }}
                        value={this.state.nombre}></TextInput>
                    <TextInput
                        label="Apellido"
                        style = {styles.textInput}
                        onChangeText={(text) => this.setState({ apellido: text })}
                        value={this.state.apellido}></TextInput>
                    <NumericInput
                        label="Teléfono"
                        style = {styles.textInput}
                        onChangeText={(text) => this.setState({ telefono: text })}
                        value={this.state.telefono} />
                    <TextInput
                        label="Email"
                        style = {styles.textInput}
                        onChangeText={(text) => this.setState({ email: text })}
                        value={this.state.email}></TextInput>

                    <PasswordInput
                        style = {styles.textInput}
                        label={"Contraseña"} onChangeText={(text) => this.setState({ pass: text })} value={this.state.pass} />
                    <PasswordInput
                         style = {styles.textInput}
                        label={"Confirmar contraseña"} onChangeText={(text) => this.setState({ passConf: text })} value={this.state.passConf} />
                </ScrollView>
                <Button label={"Registrarse"} onPress={this.trySignin} style={{ width: 150, alignSelf: 'center' }} />
            </SpinnerView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        //width: width * 0.8,
        //height: height
    },
    textInput:{
        marginHorizontal:5,
        marginVertical:5,
    }
})

const mapDispatchToProps = (dispatch) => {
    return {
        attemptSignin: (nombre, apellido, email, pass, telefono, callback) => dispatch(LoginActions.attemptSignin(nombre, apellido, email, pass, telefono, callback))
    }
};
export default connect(
    mapStateToProps, mapDispatchToProps
)(SigninScreen);