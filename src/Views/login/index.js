import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, KeyboardAvoidingView, Text, StyleSheet, Dimensions } from 'react-native'
import { appStyles, colors } from '../../index.styles';
import LoginActions from '../../redux/authState/action'
import SpinnerView from '../../../components/SpinnerView';
import { NAVIGATION } from '../../../assets/constants';
import LinkButton from '../../../components/linkButton/linkButton';
import { FontAwesome, EvilIcons } from '@expo/vector-icons';
import PerfilDrawer from '../PerfilDrawer';
import Button from '../../../components/Button/buttonPaper'
import TextInput from '../../../components/textInput/textInputPaper'
import PasswordInput from "../../../components/passwordInput/passwordPaper"
const { width, height } = Dimensions.get("window")


const mapStateToProps = (state) => {
    return {
        user: state.authState.userData
    };
}
class LoginScreen extends Component {
    state = {
        email: '',
        pass: ''
    }
    tryLogin = () => {
        this.props.attemptLogin(this.state.email, this.state.pass, () => this.setState({ email: '', pass: '' }))
    }
    trySignin = () => {
        this.props.navigation.navigate(NAVIGATION.SIGN_IN)
    }
    render() {
        return (
            <SpinnerView>
                {!this.props.user ?
                    <View style={{ width: width * 0.5 }}>

                        <EvilIcons style={{ color: colors.APP_BLUE, alignSelf: 'center', marginTop: 20 }} name={'user'} size={150} />
                        <View style={styles.data}>
                            <TextInput
                                label={"Email"} 
                                autoCapitalize='none'
                                onChangeText={(text) => this.setState({ email: text })}
                                value={this.state.email}
                                style={{}}></TextInput>
                            <PasswordInput
                                label={"Contraseña"} onChangeText={(text) => this.setState({ pass: text })} value={this.state.pass} />
                        </View>
                        <View style={styles.buttonGroup}>
                            <Button label={"Ingresar"} onPress={this.tryLogin} style={{ width: 170 }} />
                            <Text></Text>
                            <View style={styles.subButtonGroup}>
                                <View style={styles.b}>
                                    <LinkButton onPress={this.trySignin} >Registrarse</LinkButton>
                                </View>
                                <Text></Text>
                                <View style={styles.b}>
                                    <LinkButton onPress={this.trySignin} >{'Olvidé mi contraseña'}</LinkButton>
                                </View>
                            </View>
                        </View>
                    </View> : <PerfilDrawer navigation={this.props.navigation} />
                }
            </SpinnerView>
        );
    }
}

const styles = StyleSheet.create({
    data: {
        width: "100%",
        //alignContent: "center",
        //alignItems: "flex-start",
        alignSelf: "flex-start",
        //marginLeft: 2,
        marginTop: 50,
        marginBottom: 10
    },
    buttonGroup: {
        flexDirection: "column",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        // padding: 10
    },
    subButtonGroup: {
        flexDirection: "column",
        width: "100%",
        justifyContent: "space-evenly",
        alignItems: "center",
        // padding: 2,
    }
})

const mapDispatchToProps = (dispatch) => {
    return {
        attemptLogin: (email, pass, callback) => dispatch(LoginActions.attemptLogin(email, pass, callback))
    }
};
export default connect(
    mapStateToProps, mapDispatchToProps
)(LoginScreen);