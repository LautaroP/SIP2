import React from 'react';
import { View, Text } from 'react-native';
import { appStyles } from '../../index.styles';


class EmptyComponent extends React.Component {

    render() {
        let { titleText, descText, desc2Text } = this.props
        if (this.props.navigation) {
            titleText = titleText || this.props.navigation.getParam('titleText');
            descText = descText || this.props.navigation.getParam('descText');
            desc2Text = desc2Text || this.props.navigation.getParam('desc2Text');
        }

        return (
            <View style={[{ flex: 1 }, this.props.style]}>
                <Text style={[appStyles.textTitulo]}>{titleText}</Text>
                <Text style={[appStyles.textBlue]}>{descText}</Text>
                <Text style={[appStyles.textBlue]}>{desc2Text}</Text>
            </View>)
    }
}


export default EmptyComponent;