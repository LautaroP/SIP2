import React, { Component } from 'react'
import { connect } from 'react-redux'
import SpinnerView from '../../../components/SpinnerView/index'
import FormNumericInput from '../../../components/numericInput/numericInput'
import FormTextInput from '../../../components/textInput/textInput'
import { View, Alert } from 'react-native'
import { CheckBox } from 'react-native-elements'
import ComboBox from '../../../components/ComboBox/comboBox'
import { CARD_TYPES, NAVIGATION } from '../../../assets/constants'
import { appStyles } from '../../index.styles'
//import ButtonApp from '../../../components/Button/button'
import ButtonApp from '../../../components/Button/buttonPaper'
import { donarIniciativa } from '../../api/iniciativa'
export class donateScreen extends Component {
    state = {
        cardNumber: '',
        name: '',
        categoria: CARD_TYPES.VISA,
        code: '',
        date: '',
        save: false,
        amount: '0',
    }
    data = [
        {
            value: CARD_TYPES.VISA
        },
        {
            value: CARD_TYPES.MASTER_CARD
        },
        {
            value: CARD_TYPES.OTRO
        },
    ]

    componentWillMount() {
        if (!this.props.user) {
            this.props.navigation.goBack()
            this.props.navigation.toggleDrawer()
        }
    }
    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            if (!this.props.user) {
                this.props.navigation.goBack()
                this.props.navigation.toggleDrawer()
            }
        });
    }

    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }

    isValidForm() {
        //console.log(this.state)
        
        if (this.state.cardNumber.length > 0 && 
            this.state.name.length > 0 > 0  && 
            {
                //!this.state.length > 0
            } &&
            this.state.date.length > 0 && 
            this.state.amount >=0){
            return true
        }else {
                return false;
            }

    }


    parseCardNumber(nro) {
        console.log('--------------parsingCardNumber------------------')
        var s = this.deleteSpaces(nro)
        for (var i = 1; i < nro.length; i++) {
            if (i % 4 == 0 && nro[i] != ' ')
                s = s + ' '
            s = s + nro[i]
        }
        return s;
    }
    deleteSpaces(text) {
        if (!text)
            return ''
        var s = text[0]
        for (var i = 1; i < text.length; i++)
            if (text[i] != ' ')
                s = s + text[i]
        return s;
    }
    parseDate(date) {
        console.log('--------------parsingDate------------------')
        if (!date)
            return ''
        var s = date[0]
        for (var i = 1; i < date.length; i++) {
            if (i == 2 && date[i] != '/')
                s = s + '/'
            s = s + date[i]
        }
        return s;
    }
    deleteDolar(amount) {
        console.log('--------------DeleatingDolar------------------')
        return amount.replace('$', '')
    }

    donate = () => {
        donarIniciativa(this.state.amount, this.props.navigation.getParam(NAVIGATION.PARAMS.INICIATIVA), this.props.navigation.getParam('user'), (json) => {
            console.log('DOnar REsponse', json)
            if (json)
                Alert.alert(json)
            else
                Alert.alert('Error')
        })
    }
    render() {
        const { cardNumber, name, categoria, code, date, save, amount } = this.state
        console.log(this.state)
        return (
            <SpinnerView style={{ width: '90%', alignItems: 'center'  }}>
                <FormNumericInput fontSize={30} onChangeText={(text) => this.setState({ amount: this.deleteDolar(text) })} value={'$' + amount} style={{ width: '50%', alignSelf: 'center', paddingTop: 20 }} />
                <View style={appStyles.horizontalFullView}>
                    <FormNumericInput style={{ paddingRight: 10, flex: 4, marginTop: 18 }} label={"Número de tarjeta"} onChangeText={(text) => this.setState({ cardNumber: text })} value={cardNumber} />
                    <ComboBox style={{ flex: 2 }}
                        onChangeText={(categoria) =>
                            this.setState({ categoria: categoria })
                        }
                        data={this.data}
                        label={"Categoría"}
                        value={categoria}
                        onch />
                </View>
                <FormTextInput label={"Titular de la tarjeta"} onChangeText={(text) => this.setState({ name: text })} value={name} />
                <View style={{ width: "90%", flexDirection: "row", justifyContent: "space-around",}}>
                    <FormNumericInput style={{ width: 140}} label={"Vencimiento"} onChangeText={(date) => this.setState({ date: date })} value={this.parseDate(date)} />

                    <FormNumericInput style={{ width: 70 }} label={"Código"} onChangeText={(text) => this.setState({ code: text })} value={code} secureTextEntry={true} />
                </View> 
                <CheckBox checked={save} 
                    onPress={() => this.setState({ save: !save })} title='GUARDAR TARJETA' /> 
                <ButtonApp label='Donar' 
                    style= {{alignItems:'center'}}
                    onPress={()=>
                        {if (this.isValidForm()) {
                            
                            Alert.alert(
                                '',
                                '¿Desea realizar donación?',
                                [
                                  {
                                    text: 'Cancelar',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                  },
                                  {text: 'OK', onPress: this.donate},
                                ],
                                {cancelable: false},
                             )
                        } else {
                            Alert.alert(
                                '',
                                'Verifique los campos completados y el monto introducido',
                                [
                                  {
                                    text: 'Ok',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                  },
                                ],
                                {cancelable: false},
                             ) 
                        }
                    }
                    } 
                />
            </SpinnerView>

        )
    }
}

const mapStateToProps = (state) => ({
    user: state.authState.userData
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(donateScreen)
