import React from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import MapView from 'react-native-map-clustering';
import { Marker } from 'react-native-maps';

export default class Map extends React.Component {

    render() {
        return (
            <View style={styles.container}>
            <MapView
                region={{
                    latitude: -34.603598,
                    longitude: -58.381570,
                    latitudeDelta: 8.5,
                    longitudeDelta: 8.5
                }}
                style={styles.mapStyle}
            >
                <Marker coordinate={{ latitude: -34.603598, longitude: -58.381570 }} />
            </MapView>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
        padding: 10,
        height: Dimensions.get("window").height,
        width:  Dimensions.get("window").width,
    },
    title: {
        fontSize: 22,
        color: '#2699FB',
    },
    mapStyle: {
        justifyContent: 'center',
        width: ((Dimensions.get("window").width)),
        height: ((Dimensions.get("window").height)* 0.7)
        
    }
})