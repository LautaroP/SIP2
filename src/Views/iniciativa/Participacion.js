import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Button } from 'react-native-paper';
import { FontAwesome } from '@expo/vector-icons';
import { NAVIGATION } from "../../../assets/constants";
import { Linking } from 'react-native'
import SpinnerView from "../../../components/SpinnerView";
import { connect } from 'react-redux'
import { getImpulsor } from "../../api/iniciativa";



const mapStateToProps = (state) => {
    return {
        user: state.authState.userData
    };
}
class Participacion extends React.Component {
    state = {
        impulsor: undefined,
        iniciativa: this.props.navigation.getParam('iniciativa')
    };
    componentWillMount() {
        getImpulsor(this.state.iniciativa.impulsor).then(([Response, impulsor]) => {
            this.setState({ impulsor: impulsor })
        })
    }

    render() {
        const { telefono, email, nombre, apellido, titulo, descripcion } = this.state.impulsor || {}

        return (
            <SpinnerView loading={!this.state.impulsor } style={[styles.container, { marginTop: 15, backgroundColor: '#f8f8f8' }]}>
                <Text style={styles.title}>Gracias por su participación!</Text>
                <Text
                    style={{ marginTop: 30, marginLeft: 10, textAlign:'justify'}}
                >
                    A continuación te daremos los datos del organizador/a para que te pongas en contacto ante cualquier consulta:
                    </Text>

                <View style={{ alignItems: "center", marginTop: 60 }}>
                    <FontAwesome name='user-circle-o' size={45} color="#46A8FC" />
                    <Text style={{ marginTop: 5 }}>{nombre + " " + apellido}</Text>
                    <Text style={{ marginTop: 10, color: '#46A8FC' }}>{this.state.iniciativa.lugar}</Text>
                </View>


                <Text style={{ marginTop: 30, marginLeft: 10, textAlign: 'justify'}}>Hola{" " + this.props.user.nombre + " " + this.props.user.apellido + " "}, voy a estar coordinando este proyecto, no dudes en llamarme o mandarme un email para cualquier pregunta</Text>

                <View style={styles.groupButton}>
                    <Button
                        mode="outlined"
                        color='#2699FB'
                        style={styles.button}
                        onPress={() => { Linking.openURL(`tel:${telefono}`) }}
                    >
                        Llamar
                    </Button>
                    <Button
                        mode="outlined"
                        color='#2699FB'
                        style={styles.button}
                        onPress={() => Linking.openURL(`mailto:${email}`)}
                    >
                        Email
                    </Button>
                </View>
            </SpinnerView>
        )
    }
};



export default connect(mapStateToProps)(Participacion)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        //alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor: '#f8f8f8'
    },
    title: {
        fontSize: 22,
        textAlign: "center",
        color: '#2699FB',
        paddingTop:15,
    },
    groupButton: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: "center",
        marginTop: 100,
    },
    button: {
        width: 100,
        borderWidth: 2,
        borderColor: '#2699FB'
    }
})