import { View, ScrollView, Text, StyleSheet, KeyboardAvoidingView, FlatList, TextInput, Dimensions, Alert, Keyboard } from 'react-native';
import { appStyles, colors } from '../../index.styles';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getComentarios, postComentario } from '../../api/iniciativa';
import { FontAwesome } from '@expo/vector-icons'

const { width, height } = Dimensions.get('window');

function mapStateToProps(state) {
    return {
        user: state.authState.userData,
    };
}

function mapDispatchToProps(dispatch) {
    return {

    };
}





class ComentariosScreen extends Component {
    state = {
        comentarios: [],
        msg: '',
        keyboardShown: false,
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    componentWillMount() {
        this.getComments()
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                this.setState({ keyboardShown: true })
            },

        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                this.setState({ keyboardShown: false })
            },
        );
    }
    getComments = () => {
        const iniciativa = this.props.data;
        getComentarios(iniciativa._id).then(([comentarios]) => this.setState({ comentarios: comentarios }))

    }
    send = (user, id, msg) => {
        if (user) {
            var comment = msg.nativeEvent.text
            const mail = user.email
            console.log("coment: " + comment);
            console.log("mail: " + mail);
            console.log("id: " + id);


            const requestBody = {
                mail: mail,
                idIniciativa: id,
                comment: comment,
            }
            postComentario(requestBody).then(() => {
                this.getComments()
                this.setState({ msg: msg })
            })
        } else {
            this.props.navigation.toggleDrawer()
        }
    }
    render() {
        const comentarios = this.state.comentarios;
        return (
            <View style={[styles.container, { height: (this.state.keyboardShown ? "50%" : "100%") }]}>
                {comentarios.length != 0 ?
                    <View style={{ flex: 1 }} >
                        <ScrollView style={{ flex: 1, width: '100%' }} contentContainerStyle={styles.scrollContainer} centerContent={true}>
                            {comentarios.sort(compareComentarios).map((comentario) => <View style={styles.card}>
                                <View style={styles.commentsBox}>
                                    <FontAwesome name='user-circle' size={40} color={colors.APP_BLUE} />
                                    <View style={{ flex: 1, marginLeft: 10 }}>
                                        <View style={[appStyles.horizontalFullView, { alignItems: 'center', justifyContent: 'space-between', }]}>
                                            <Text style={appStyles.textoPlanoGrisSmall}>
                                                {comentario.email}
                                            </Text>
                                            <Text style={appStyles.textoPlanoGris}>
                                                {getFecha(comentario.fecha)}
                                            </Text>
                                        </View>
                                        <Text style={[appStyles.textBlue, { textAlign: "left" }]}>
                                            {comentario.comentario}
                                        </Text>
                                    </View>
                                </View>
                            </View>)}
                        </ScrollView>
                    </View>

                    :
                    <View style={styles.noComments}>
                        <Text style={styles.textNoComments}>Esta iniciativa no tiene comentarios.</Text>
                    </View>
                }
                <View style={styles.input}>
                    <TextInput
                        style = {{marginHorizontal:10}}
                        value={this.state.msg}
                        placeholderTextColor="#696969"
                        onChangeText={msg => this.setState({ msg: msg })}
                        blurOnSubmit={false}
                        onSubmitEditing={(msg) => this.send(this.props.user, this.props.data._id, msg)}
                        inlineImageLeft='search_icon'
                        placeholder="Escriba su comentario aquí..."
                        returnKeyType="send" />
                </View>
            </View>
        );
    }
}
compareComentarios = (a, b) => {
    if (a.fecha > b.fecha) {
        return -1;
    }
    if (b.fecha > a.fecha) {
        return 1;
    }
    return 0;
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        flexDirection: "column",
        width: "100%",
        alignItems: "center",
        backgroundColor: '#f8f8f8'
    },
    scrollContainer: {
        // flex: 1,
        width: "100%",
        alignItems: "center"
    },
    card: {
        width: "80%",
        borderWidth: 0.5,
        borderColor: colors.APP_LIGHT_BLUE,
        borderRadius: 20,
        padding: 10,
        marginVertical: 10,
    },
    commentsBox: {
        width: '100%',
        flexDirection: 'row',
    },
    noComments: {
        flex: 1,
        justifyContent: "center"
    },
    textNoComments: {
        color: colors.APP_LIGHT_BLUE,
        textAlign: 'center',
        fontSize: 30,
        fontWeight: "bold",
        //marginTop: 15
    },
    keyboard: {
        flex: 1,
        justifyContent: 'center',
    },
    input: {
        width: '90%',
        borderWidth: 1,
        borderColor: colors.APP_LIGHT_BLUE,
        borderRadius: 20,
        padding: 10,
        marginBottom:10,
    },
})

const getFecha = (fecha) => {
    const date = new Date(fecha)
    return (((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear())
}
export default connect(
    mapStateToProps, mapDispatchToProps
)(ComentariosScreen);

