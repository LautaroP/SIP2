import React from "react";
import { View, Text, StyleSheet, Dimensions, ImageBackground } from "react-native";
import ProgressCircle from 'react-native-progress-circle';
import { LinearGradient } from 'expo-linear-gradient';

export default class Metricas extends React.Component {

    state = {
        objetivoPersonaEsperado: null,
        objetivoPersonaAcumulado: null,
        objetivoEconomicoEsperado: null,
        objetivoEconomicoAcumulado: null,
        fechaLimite: null,

        percentVoluntarios: null,
        percentDonaciones: null,
        diasRestante: null,
    };

    componentDidMount() {
        this.setState({
            objetivoPersonaEsperado: this.props.objetivoPersonaEsperado,
            objetivoPersonaAcumulado: this.props.objetivoPersonaAcumulado,
            objetivoEconomicoEsperado: this.props.objetivoEconomicoEsperado,
            objetivoEconomicoAcumulado: this.props.objetivoEconomicoAcumulado,
            fechaLimite: this.props.fechaLimite,
        });
        if (this.props.objetivoPersonaEsperado !== 0)
            this.setState({ percentVoluntarios: this.props.objetivoPersonaAcumulado / this.props.objetivoPersonaEsperado * 100 });
        if (this.props.objetivoEconomicoEsperado !== 0)
            this.setState({ percentDonaciones: this.props.objetivoEconomicoAcumulado / this.props.objetivoEconomicoEsperado * 100 });
        if (this.props.fechaLimite) {
            var diff = new Date(this.props.fechaLimite).getTime() - new Date().getTime();    //fecha limite - fecha actual
            var dias = Math.floor(diff / (1000 * 60 * 60 * 24));
            this.setState({ diasRestante: dias })
        }
    }

    render() {
        const { objetivoPersonaEsperado, objetivoPersonaAcumulado, objetivoEconomicoEsperado,
            objetivoEconomicoAcumulado, fechaLimite, percentVoluntarios, percentDonaciones, diasRestante } = this.state;
            console.log(this.state)

        return (
            <View style={styles.container}>
                {objetivoPersonaEsperado ?
                    <View style={styles.box}>
                        <LinearGradient style={styles.boxHeader} colors={['#8cc2fc', '#6eb4ff', '#2699FB']}>
                            <Text style={styles.title}>Voluntarios</Text>
                        </LinearGradient>
                        
                        <ImageBackground source={require('./img/mv.png')}
                        style={{width: '100%',flex:1}}
                        imageStyle={styles.imageBackground}
                        >
                            <View style={styles.boxContent}>
                            <ProgressCircle
                                percent={percentVoluntarios}
                                radius={47}
                                borderWidth={8}
                                color="#3E606F"
                                shadowColor="#DCEEFF"
                                bgColor="#fff"
                            >
                                <Text style={styles.circleNum}>{objetivoPersonaAcumulado + '/' + objetivoPersonaEsperado}</Text>
                            </ProgressCircle>
                            <View style={styles.boxText}><Text>Cantidad de personas inscriptas para colaborar con la causa propuesta por la iniciativa.</Text></View>
                            </View>
                        </ImageBackground>

                    </View>
                    : <View style={styles.box} >
                        <LinearGradient style={styles.boxHeader} colors={['#8cc2fc', '#6eb4ff', '#2699FB']}>
                            <Text style={styles.title}>Voluntarios</Text>
                        </LinearGradient>

                        <ImageBackground source={require('./img/mv.png')}
                        style={{width: '100%',flex:1}}
                        imageStyle={styles.imageBackground}
                        >
                        <View style={styles.boxContent}>
                            <View style={styles.boxText}><Text>Aún no se registraron personas</Text></View>
                        </View>
                        </ImageBackground>

                    </View>
                }

                {objetivoEconomicoEsperado ?
                    <View style={styles.box}>
                        <LinearGradient style={styles.boxHeader} colors={['#8cc2fc', '#6eb4ff', '#2699FB']}>
                            <Text style={styles.title}>Objetivo Económico</Text>
                        </LinearGradient>

                        <ImageBackground source={require('./img/mpc.png')}
                        style={{width: '100%',flex:1}}
                        imageStyle={styles.imageBackground}
                        >
                        <View style={styles.boxContent}>
                            <ProgressCircle
                                percent={percentDonaciones}
                                radius={47}
                                borderWidth={8}
                                color="#FEE444"
                                shadowColor="#DCEEFF"
                                bgColor="#fff"
                            >
                                <Text numberOfLines={1} style={styles.circleNum}>{objetivoEconomicoAcumulado + '/' + objetivoEconomicoEsperado}</Text>
                            </ProgressCircle>
                            <View style={styles.boxText}><Text>Monto total recaudado hasta la fecha.</Text></View>
                        </View>
                        </ImageBackground>

                    </View>
                    : <View style={styles.box}>
                        <LinearGradient style={styles.boxHeader} colors={['#8cc2fc', '#6eb4ff', '#2699FB']}>
                            <Text style={styles.title}>Objetivo Económico</Text>
                        </LinearGradient>

                        <ImageBackground source={require('./img/mpc.png')} 
                        style={{width: '100%',flex:1}}
                        imageStyle={styles.imageBackground}
                        >
                        <View style={styles.boxContent}>
                            <View style={styles.boxText}><Text>No se estableció un objetivo económico</Text></View>
                        </View>
                        </ImageBackground>

                    </View>
                }

                {fechaLimite ?
                    <View style={styles.box}>
                        <LinearGradient style={styles.boxHeader} colors={['#8cc2fc', '#6eb4ff', '#2699FB']}>
                            <Text style={styles.title}>Finalización</Text>
                        </LinearGradient>

                        <ImageBackground source={require('./img/mt.png')}
                        style={{width: '100%',flex:1}}
                        imageStyle={styles.imageBackground}
                        >
                        <View style={styles.boxContent}>
                            <ProgressCircle
                                percent={100}
                                radius={47}
                                borderWidth={8}
                                color="#ED8E46"
                                shadowColor="#DCEEFF"
                                bgColor="#fff"
                            >
                                <Text style={styles.circleNum}>{diasRestante + ' dias'}</Text>
                            </ProgressCircle>
                            <View style={styles.boxText}>
                                <Text>Días restantes para que la iniciativa llegue a su fin.</Text>
                            </View>
                        </View>
                        </ImageBackground>

                    </View>
                    : <View style={[styles.box, {}]}>
                        <LinearGradient style={styles.boxHeader} colors={['#8cc2fc', '#6eb4ff', '#2699FB']}>
                            <Text style={styles.title}>Finalización</Text>
                        </LinearGradient>

                        <ImageBackground source={require('./img/mt.png')}
                        style={{width: '100%',flex:1}}
                        imageStyle={styles.imageBackground}
                        >
                        <View style={styles.boxContent}>
                            <View style={styles.boxText}><Text>No se definió una fecha de finalización.</Text></View>
                        </View>
                        </ImageBackground>

                    </View>
                }

            </View>
        )
    }
};


var { height, width } = Dimensions.get('window');

//var box_width = width*0.8

const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection: 'column',
        backgroundColor:'#f8f8f8',
    },
    box: {
        flex:1,
        width: '100%',
        alignItems: "center",
        backgroundColor: 'white',
    },
    boxHeader: {
        width: "100%",
        backgroundColor: '#E7E7E7',
        alignItems: 'center',
        justifyContent: "center",
    },
    boxContent: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
       // justifyContent: "space-between",
        width: '100%',
        padding: 10,
    },
    boxText: {
        width: "45%",
        //marginLeft: 2,
        padding: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    title: {
        fontSize: 18,
        color: 'white',
    },
    circleNum: {
        fontSize: 10,
        color: "#000"
    },
    box1: {
        alignSelf: 'flex-start',
    },
    box2: {
        alignSelf: 'flex-end',
    },
    box3: {
        alignSelf: 'flex-start',

    },
    imageBackground: {
        //resizeMode: 'stretch',
    }
})