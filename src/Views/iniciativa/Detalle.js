import React from "react";
import { View, Text, StyleSheet, Image, Dimensions, Linking, Platform, Alert } from "react-native";
import { IconButton } from 'react-native-paper';
import { NAVIGATION } from "../../../assets/constants";
import { appStyles, colors } from "../../index.styles";
//import Button from '../../../components/Button/button';
import Button from '../../../components/Button/buttonPaper';
import { connect } from 'react-redux';
import { inscribirse, inscribirseEnIniciativa } from '../../api/iniciativa'
import AuthActions from '../../redux/authState/action'



const mapStateToProps = (state) => {
    return {
        user: state.authState.userData
    };
}
class Detalle extends React.Component {

    state = {
        titulo: '',
        descripcion: '',
        objetivoPersonaEsperado: "",
        objetivoEconomicoEsperado: "",
        iniciativa: undefined,

    };

    componentDidMount() {
        this.setState({
            titulo: this.props.titulo,
            descripcion: this.props.descripcion,
            objetivoPersonaEsperado: this.props.objetivoPersonaEsperado,
            objetivoEconomicoEsperado: this.props.objetivoEconomicoEsperado,
            iniciativa: this.props.iniciativa,
        })
    };

    random() {
        var array = ['https://picsum.photos/100', 'https://picsum.photos/200', 'https://picsum.photos/300', 'https://picsum.photos/400', 'https://picsum.photos/500', 'https://picsum.photos/600', 'https://picsum.photos/700', 'https://picsum.photos/800', 'https://picsum.photos/900']
        return array[Math.floor(Math.random() * (8))]
    }

    inscribirse = () => {
        if (this.props.user)
            inscribirseEnIniciativa(this.state.iniciativa, this.props.user, (response) => {
                console.log('Response Inscripcion:', response);
                switch(response.status){
                    case 200: 
                        Alert.alert('Inscripción Realizada');
                        this.props.navigation.navigate(NAVIGATION.PARTICIPACION, { iniciativa: this.state.iniciativa });
                        break;       
                    case 401: 
                        Alert.alert('Ya participa en esta iniciativa');
                        break;
                    default: 
                        Alert.alert('Error en la inscripcion');
                        break;
                }
            })
        else
            this.props.navigation.toggleDrawer()

    }

    render() {
        const { titulo, descripcion, objetivoPersonaEsperado, objetivoEconomicoEsperado } = this.state;
        return (
            <View style={{ flex: 1, backgroundColor: '#f8f8f8', }}>
                <View style={{ flex: 3 }}>
                    <Image style={styles.image} source={{ uri: this.random() }} />
                </View>
                <View style={[styles.container, { marginTop: 20 }]}>
                    <Text style={styles.title}>{titulo}</Text>
                    <Text style={{ marginTop: 20 }}>{descripcion}</Text>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: "flex-end" }}>

                        {
                        /*
                            <Button
                            label={"Ver en maps"}
                            style={{ width: 140 }}
                            onPress={(lat, lng) => {
                                var scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:';
                                var url = scheme + `${-34.6037345},${-58.3837591}`;
                                Linking.openURL(url);
                            }}
                        />*/
                        }

                        <Text />
                        <View style={{ width: '100%', flexDirection: 'row', alignContent: 'space-around', alignItems: 'center', justifyContent: 'space-evenly' }}>
                            {(!!objetivoPersonaEsperado &&
                                (!this.props.user || this.props.user.email != this.props.iniciativa.impulsor)) && <Button
                                    label={"colaborar"}
                                    onPress={() =>
                                        Alert.alert(
                                            '',
                                            '¿Desea inscribirse en la actividad?',
                                            [
                                                {
                                                    text: 'Cancelar',
                                                    onPress: () => console.log('Cancel Pressed'),
                                                    style: 'cancel',
                                                },
                                                { text: 'OK', onPress: this.inscribirse },
                                            ],
                                            { cancelable: false },
                                        )
                                    }
                                ></Button>
                            }
                            {(!!objetivoEconomicoEsperado &&
                                (!this.props.user || this.props.user.email != this.props.iniciativa.impulsor)) && <Button
                                    label={"donar"}
                                    onPress={() => { this.props.navigation.navigate(NAVIGATION.DONATE, { [NAVIGATION.PARAMS.INICIATIVA]: this.state.iniciativa, user: this.props.user }) }}
                                >
                                </Button>
                            }
                        </View>
                    </View>
                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 7,
        alignItems: 'center',
        //justifyContent: 'center',
    },
    title: {
        fontSize: 22,
        color: '#2699FB',
    },
    buttons: {
        borderColor: "#2099FB",
        borderRadius: 10,
        borderWidth: 2,
        width: 140
    },
    image: {
        height: "100%",
        marginRight: 6,
        marginLeft: 6,
        marginBottom: 5,
        borderWidth: 0.5,
        borderRadius: 8,
        borderColor: "black",
    },
})
const mapDispatchToProps = (dispatch) => {
    return {
        reLogin: () => dispatch(AuthActions.reLogin()),
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(Detalle)
