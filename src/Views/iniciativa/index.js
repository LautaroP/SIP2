import React from "react";
import { View, Text, StyleSheet, Dimensions, ScrollView } from "react-native";
import Swiper from "react-native-web-swiper";
import Detalle from './Detalle';
import Metricas from './Metricas';
import Map from './Map'
import { NAVIGATION } from '../../../assets/constants';
import ComentariosScreen from "./comentarios";

export default class Screen extends React.Component {

    state = {
        iniciativa: this.props.navigation.getParam(NAVIGATION.PARAMS.INICIATIVA_SELECTED, undefined),
    };
    render() {
        
        return (
            <View style={styles.container}>
                {
                    this.state.iniciativa
                        ? <Swiper
                            swipeAreaStyle= {styles.swipper}
                            controlsProps={{ prevTitle: '', nextTitle: '', dotsTouchable: true, }}
                            actionMinWidth="0.25"
                            slideWrapperStyle={styles.slideContainer} >
                            <ScrollView style={styles.slideContainer} contentContainerStyle={styles.detailsSlide}>
                                <Detalle
                                    navigation={this.props.navigation}
                                    titulo={this.state.iniciativa.titulo}
                                    descripcion={this.state.iniciativa.descripcion}
                                    iniciativa={this.state.iniciativa}
                                    objetivoEconomicoEsperado={this.state.iniciativa.objetivoEconomicoEsperado}
                                    objetivoPersonaEsperado={this.state.iniciativa.objetivoPersonaEsperado}
                                />
                            </ScrollView>
                            <View style={styles.slideContainer}>
                                <Metricas
                                    objetivoPersonaAcumulado={this.state.iniciativa.objetivoPersonaAcumulado}
                                    objetivoPersonaEsperado={this.state.iniciativa.objetivoPersonaEsperado}
                                    objetivoEconomicoAcumulado={this.state.iniciativa.objetivoEconomicoAcumulado}
                                    objetivoEconomicoEsperado={this.state.iniciativa.objetivoEconomicoEsperado}
                                    fechaLimite={this.state.iniciativa.fechaLimite}
                                />
                            </View>
                            {/*<View style={styles.slideContainer, styles.mapSlide}>
                                <Map />
                            </View>*/}
                            <View style={{ flex: 1 }}>
                                <ComentariosScreen data={this.state.iniciativa} navigation={this.props.navigation} />
                            </View>
                        </Swiper>
                        : <Text>Cargando...</Text>
                }
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#f8f8f8',
    },
    slideContainer: {
        flex: 1,
        height:'95%',
        marginBottom:30,
        // alignItems: "center",
        // padding: 5,
        //justifyContent: "center",
        //width: "80%",
        //height: "90%"
    },
    detailsSlide: {
        flex: 1,
        alignItems: "center",
        padding: 15,
    },
    metricsSlide: {
        //padding: 15,
    },
    mapSlide: {
        flex: 1,
        padding: 15,
        alignItems: "center",
    },
    swipper: {
        flex:1,
    }

});