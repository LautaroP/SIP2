import React, { Component } from 'react';
import { Platform, StyleSheet, View, ScrollView, Text, ActivityIndicator } from 'react-native';
import { ProgressBar, Colors, Avatar, Button, Card, Title, Paragraph, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { NAVIGATION } from '../../../assets/constants';
import { appStyles } from '../../index.styles';
import IniciativasList from '../../../components/iIniciativasList/iniciativasList';
import { connect } from 'react-redux'
class MisColaboraciones extends Component {

  state = {
    initiativeExistence: null,
    iniciativas: '',
  }

  componentDidMount() {
    const categoria = this.props.navigation.getParam(NAVIGATION.PARAMS.CATEGORIA, undefined);
    this.setState({ categoria: categoria });
    let requestBody = {};
    requestBody.mail = this.props.user.email,
      fetch('https://still-beach-77233.herokuapp.com/rutes/showColaboraciones', {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
      })
        .then(response => {
          if (response.status === 200) {
            this.setState({ initiativeExistence: true });
            return response.json();
          }
          else {
            this.setState({ initiativeExistence: false });
            return response.json();
          }
        })
        .then(
          (result) => {
            this.setState({ iniciativas: result });
          }
        )
  };



  render() {
    const { initiativeExistence, iniciativas } = this.state;
    return (

      <IniciativasList navigation={this.props.navigation} data={iniciativas} dataExists={initiativeExistence} />
    );
  }
};

const mapStateToProps = (state) => {
  return {
    user: state.authState.userData
  }
};
export default connect(
  mapStateToProps)(MisColaboraciones);