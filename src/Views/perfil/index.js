
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import { colors } from '../../index.styles';
import { NAVIGATION } from '../../../assets/constants';
import { EvilIcons } from '@expo/vector-icons';
import { connect } from 'react-redux';

class Perfil extends React.Component {
    componentWillMount() {
        if (!this.props.user) {
            this.props.navigation.navigate(NAVIGATION.MAIN_MENU)
            this.props.navigation.toggleDrawer()
        }
    }
    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            if (!this.props.user) {
                this.props.navigation.navigate(NAVIGATION.MAIN_MENU)
                this.props.navigation.toggleDrawer()
            }
        });
    }
    render() {
        const { colaboraciones, impulsadas, email, password, nombre, apellido, telefono } = this.props.user || {}
        return (
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
                <View style={styles.header}></View>
                <View style={styles.avatar}>
                    <EvilIcons style={{ color: colors.APP_BLUE, marginTop: 10 }} name={'user'} size={120} />
                </View>

                {/*
                <Image style={styles.avatar} source={require('./img/avatar.png')} />
                */ }
                <View style={styles.body}>
                    <View style={styles.bodyContent}>

                        <View style={styles.profileDetail}>
                            <View style={styles.detailContent}>
                                <Text style={styles.title}>Iniciativas</Text>
                                <Text style={styles.count}> {impulsadas ? impulsadas.length : 0}</Text>
                            </View>
                            <View style={styles.detailContent}>
                                <Text style={styles.title}>Colaboraciones</Text>
                                <Text style={styles.count}>{colaboraciones ? colaboraciones.length : 0}</Text>
                            </View>
                        </View>

                        <Text style={styles.name}>{nombre + " " + apellido}</Text>
                        <Text style={styles.info}>{email}</Text>
                        <View style={{ marginTop: 120 }}>
                            <TouchableOpacity style={styles.touchable}>
                                <Image style={styles.icon} source={require('./img/iniciativas.png')} />
                                <Text style={styles.opcions}>Mis iniciativas</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.touchable}
                                onPress={() => this.props.navigation.navigate(NAVIGATION.MISCOLABORACIONES)}
                            >
                                <Image style={styles.icon} source={require('./img/colaboraciones.png')} />
                                <Text style={styles.opcions}>Mis colaboraciones</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.authState.userData
    };
}

export default connect(mapStateToProps)(Perfil)

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#2099FB',
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 6,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 100,
        zIndex: 1,
        backgroundColor: 'white',
    },
    body: {
        position: 'absolute',
        alignSelf: 'center',
        width: 350,
        borderRadius: 38,
        marginTop: 170,
        paddingTop: 30,
        zIndex: 0,
        backgroundColor: '#F9F9F9',
    },
    bodyContent: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center',
        padding: 30,
        marginBottom: 100,
        backgroundColor: '#F9F9F9',
    },
    name: {
        fontSize: 28,
        color: "#696969",
        fontWeight: "600"
    },
    info: {
        fontSize: 16,
        color: "#2099FB",
        marginTop: 10
    },
    opcions: {
        fontSize: 16,
        color: "black",
    },
    description: {
        fontSize: 16,
        color: "#696969",
        marginTop: 10,
        textAlign: 'center'
    },
    icon: {
        width: 30,
        height: 30,
        marginTop: 2,
        marginRight: 10,
    },
    touchable: {
        marginTop: 25,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 250,
        borderRadius: 30,
        backgroundColor: "white",
    },
    profileDetail: {
        alignSelf: 'center',
        marginTop: 120,
        alignItems: 'center',
        flexDirection: 'row',
        position: 'absolute',
        backgroundColor: '#ffffff',
        borderRadius: 30,
    },
    detailContent: {
        margin: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 16,
        color: "black",
    },
    count: {
        fontSize: 18,
        color: '#2099FB',
    },
});