import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Platform,
    StatusBar,
    ScrollView,
    Image,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
} from "react-native";
import {
    Colors,
    Snackbar,
    DefaultTheme,
    Card,
    Title,
    Paragraph,
    ProgressBar,
    Caption,

} from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons'
import Category from './Category'
import secureFetch from "../../api/secureFetch"
import { NAVIGATION } from '../../../assets/constants';
import { Col } from "native-base";
import NewsApi from "../../api/NewsApi";
import Noticias from './Noticias'
//import {colors} from "../../src/index.styles"

const { height, width } = Dimensions.get('window')

class Explore extends Component {

    state = {
        visible: true,
        iniciativas:null,
        news: null
    }

    componentDidMount() {
        this.startHeaderHeight = 80
        if (Platform.OS == 'android') {
            this.startHeaderHeight = 70 + StatusBar.currentHeight
        }
     
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            secureFetch('https://still-beach-77233.herokuapp.com/rutes/getIniciativasTopTen')
                .then(([response, json]) => {
                    this.setState(({ iniciativas: json }))
                }).catch()
        });


        NewsApi.getNews().then((result) =>{
            this.setState(({ news: result }))
        }).catch((err) => console.log(err))
  
    }

    dateDiff(origDate) {
        var diff = new Date(origDate).getTime() - new Date().getTime();    //fecha limite - fecha actual
        var dias = Math.floor(diff / (1000 * 60 * 60 * 24));
        dias = (dias * -1)
        //console.log(dias);

        return (dias * -1)
    }

    eventPerc(event) {
        var acumPer = event.objetivoPersonaAcumulado
        var espPer = event.objetivoPersonaEsperado
        var acumMonto = event.objetivoEconomicoAcumulado
        var espMonto = event.objetivoEconomicoEsperado
        var perc = 0

        if (acumPer && espPer) {
            perc = acumPer / espPer
            //return (Math.round(perc * 10) / 10)
        } else {
            if (acumMonto && espMonto) {
                perc = acumMonto / espMonto
                //return (Math.round(perc * 100) / 100)
            } else {
                perc = 0
                //return (Math.round(perc * 100) / 100)
            }
        }
        if (perc > 0.04) {
            perc = (Math.round(perc * 10) / 10)
        } else {
            perc = (Math.round(perc * 100) / 100)
        }

        //console.log("fixed: " + perc);

        return perc

    }

    random() {
        var array = ['https://picsum.photos/100', 'https://picsum.photos/200', 'https://picsum.photos/300', 'https://picsum.photos/400', 'https://picsum.photos/500', 'https://picsum.photos/600', 'https://picsum.photos/700', 'https://picsum.photos/800', 'https://picsum.photos/900']
        return array[Math.floor(Math.random() * (8))]
    }

    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }
    render() {

        const data = this.state.iniciativas;
        const news = this.state.news;
        console.log(news);
        
        return (
                    <ScrollView
                        scrollEventThrottle={16}
                    >
                        <View style = {styles.mainScrollContainer}>
                            <View  style={styles.newEventsContainer}>
                            <Text style={{ flex:1, fontSize: 15, fontWeight:'600', paddingLeft: 5, color: Colors.grey600, borderBottomColor: Colors.grey300, borderBottomWidth:1, marginVertical:10, paddingBottom:5}}>
                                Últimas iniciativas creadas
                            </Text>
                            {
                                !data ? (
                                    <View
                                    style={{
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginTop: 50,
                                        flex:1,
                                    }}>
                                    <ActivityIndicator
                                        size="large"
                                        color={Colors.blue600}
                                        style={{ flex: 1 }}
                                    />
                                    </View>) 
                                : (
                                    <View style={styles.scrollView}>
                                        <ScrollView
                                            horizontal={true}
                                            showsHorizontalScrollIndicator={false}>
                                            {
                                                data.map(item => (
                                                    <TouchableOpacity
                                                    onPress={() =>
                                                        this.props.navigation.navigate(NAVIGATION.INICIATIVA, {
                                                        [NAVIGATION.PARAMS.INICIATIVA_SELECTED]: item,
                                                        })
                                                    }>
                                                    <Card style={styles.eventContainer}>
                                                        <Card.Content />
                                                        <Card.Cover
                                                            style={styles.eventImage}
                                                            source={{ uri: this.random() }}
                                                        />
                                                        <Title style={styles.eventPreviewtitle}>
                                                        {item.titulo}
                                                        </Title>
                                                        <View style={styles.statusContainerIniciativas}>
                                                            <Caption style={styles.description2}>
                                                                Objetivo: {this.eventPerc(item) * 100} %
                                                            </Caption>
                                                            <ProgressBar
                                                                style={styles.progressBar}
                                                                progress={this.eventPerc(item)}
                                                                color={
                                                                this.eventPerc(item) > 0 && this.eventPerc(item) < 0.3
                                                                    ? Colors.red800
                                                                    : this.eventPerc(item) < 0.6
                                                                    ? Colors.yellow800
                                                                    : Colors.green500
                                                                }
                                                            />
                                                        </View>
                                                    </Card>
                                                    </TouchableOpacity>
                                                ))
                                            }
                                        </ScrollView>
                                    </View>
                                )
                            }  
                            </View>
                            <View style={styles.newsContainer}>
                            <Text style={{ flex:1, fontSize: 15, fontWeight:'600', padding: 5, color: Colors.grey700, borderBottomWidth:0.5, borderBottomColor:Colors.grey200 }}>
                               Noticias
                            </Text>
                            {
                                !news ? (
                                    <View
                                    style={{
                                        flexDirection: 'column',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        alignContent: 'center',
                                        marginTop: 50,
                                        flex:1,
                                    }}>
                                    <ActivityIndicator
                                        size="large"
                                        color={Colors.blue600}
                                        style={{ flex: 1 }}
                                    />
                                    </View>) 
                                : (
                                    <View style={styles.scrollView}>
                                        <ScrollView >
                                            {
                                                news.map(item => (
                                                    <View style = {styles.cardContainer}>
                                                        <Card style={styles.mainContainer}>
                                                        <Card.Content />
                                                            <Title style={styles.title}>{item.title}</Title>
                                                            <Card.Cover
                                                            style={styles.image}
                                                            source={{ uri: item.photo }}
                                                            /> 
                                                            <Paragraph style={styles.description}>
                                                                {item.body}
                                                            </Paragraph>
                                                            
                                                    </Card> 
                                                    </View>
                                                    

                                                ))
                                            }
                                        </ScrollView>
                                    </View>
                                )
                            }  
                            </View>                      
                        </View>
                    </ScrollView>          
        );
    }
}
export default Explore;



const styles = StyleSheet.create({
    bodyContainer: {
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 20,
    },
    news: {
        width: width - 40,
        height: 200,
        marginTop: 20,
    },
    scrollView: {
        backgroundColor:'#f8f8f8',
        flex:1,
    },
    //--- Events ----
    mainContainerIniciativas: {
        borderColor: '#DCEEFF',
        borderWidth: 1,
        borderRadius: 8,
        width: 100,
        margin: 5,
    },
    imageIniciativas: {
        //borderTopStartRadius: 8,
        height: 30,
        justifyContent: "flex-start"
        //marginBottom: 5,
        //marginHorizontal: 5
    },
    titleIniciativas: {
        //marginTop: 15,
        //marginBottom: 5,
        //marginLeft: 10,
        fontSize: 10,
        fontWeight: "bold",
        textAlign: "center",
        lineHeight: 10,
        color: Colors.black,
    },
    descriptionIniciativas: {
        //margin: 10,
        color: Colors.grey700,
    },
    progressContainerIniciativas: {
        display: 'flex',
        flexDirection: 'column',
    },
    statusContainerIniciativas: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent:'space-between',
        flex:1,
    },
    countdownIniciativas: {
        // marginLeft: 15,
        //marginBottom: -5,
        color: Colors.grey600,
    },
    statusIniciativas: {
        // marginRight: 15,
        // marginBottom: -5,
        color: Colors.grey600,
        alignSelf: 'flex-end',
    },
    progressBarIniciativas: {
         margin: 10,
    },
    //--- News -----
    actionHolder: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 10,
    },
    caption: {
        marginLeft: 10,
        marginTop: 5,
        fontSize: 10,
    },
    description: {
        marginLeft: 10,
        marginBottom: 10,
        marginRight: 10,
        color: Colors.grey600,
        textAlign:'justify'
    },
    description2: {
        marginLeft: 10,
        marginBottom:3,
        flex:1,
        color: Colors.grey800,
    },
    mainContainer: {
        borderColor: Colors.grey300,
        borderWidth: 1,
        borderRadius: 6,
        backgroundColor: Colors.white,
    },
    image: {
        height: 140,
        marginBottom: 5,
        padding:1,
        //padding: 10
    },
    title: {
        marginHorizontal: 15,
        color: Colors.grey700,
        fontSize: 15, fontWeight:'600',
        lineHeight:19,
        marginBottom:10,
    },

    eventContainer: {
        display:'flex',
        flexDirection:'column',
        borderColor: Colors.grey200,
        borderWidth: 1,
        width:130,
        margin: 10,
        height:190,
        zIndex:4,

      },
      eventImage: {
        flex:2,
        marginTop:-16,
        justifyContent:'flex-start',
        borderTopEndRadius: 4,
        borderTopStartRadius:4,
        zIndex:-1,

      },
      progressBar: {
        marginBottom:10,
        marginHorizontal:10,
        paddingTop:-5,
        height:10,
      },
      eventPreviewtitle: {
        marginTop:5,
        marginHorizontal:5,
        flex:1,
        textAlign:'center',
        justifyContent:'center',
        alignContent:'center',
        alignSelf:'center',
        fontSize:14,
        lineHeight:16,
        marginBottom:-5,
      },
      newsContainer: {
          flex:4,
          paddingHorizontal: 10 ,
          backgroundColor:'#f8f8f8',

      },
      mainScrollContainer: {
          flex:1,
          backgroundColor:'#f8f8f8',
      },
      newEventsContainer: {
          borderColor: '#f8f8f8',
          marginBottom:10,
          marginTop:15,
          paddingHorizontal:10,
          backgroundColor:'#f8f8f8',
          shadowColor:Colors.grey500,
          zIndex:4,

        },
        cardContainer: {
            backgroundColor:'#f8f8f8',
            paddingTop: 10,
            paddingBottom: 10,
            borderRadius:4,
        }
});
const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
        ...DefaultTheme.colors,
        primary: Colors.blue400,
        accent: "#FDDB32",
        //text: Colors.red500,
    },
};