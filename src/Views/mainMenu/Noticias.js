import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
} from "react-native";
import {
    Colors,
    Card,
    Title,
    Paragraph,

} from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons'
import Category from './Category'
import secureFetch from "../../api/secureFetch"
import { NAVIGATION } from '../../../assets/constants';

class Noticias extends Component {
    render(){
        return(
            <View style={{ marginTop: 40, paddingHorizontal: 20 }}>
                                <Text style={{ fontSize: 20, fontWeight: '700' }}>
                                    Noticias
                                </Text>

                                
                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>¿Sabías que…?</Title>
                                        <Paragraph style={styles.description}>
                                            la temperatura global y el nivel de los mares han aumentado de forma más acelerada que nunca en la historia de la tierra, durante el último siglo.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/100' }}
                                        />
                                        
                                    </Card>

                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>Un jubilado repara la plaza de su barrio para que la puedan disfrutar los chicos.</Title>
                                        <Paragraph style={styles.description}>
                                            Un hombre de 76 años eligió asumir una tarea que las autoridades de la ciudad de Corrientes decidieron ignorar: mantener y reparar la plaza de su barrio. Don Lito pasa sus tardes cortando el pasto o reparando las hamacas y otros elementos de este predio. Incluso se ha encargado de arreglar algunas veredas. Su familia y vecinos alabaron su predisposición solidaria.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/200' }}
                                        />
                                        
                                    </Card>

                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>Con 16 años impulsó un proyecto solidario formado por más de 30 adolescentes de distintos colegios.</Title>
                                        <Paragraph style={styles.description}>
                                            Hacía tiempo que Gabriel Iglesias tenía ganas de involucrarse en alguna actividad solidaria: ayudar a niños en situación de vulnerabilidad. Lo que no llegó a vislumbrar fueron a los 30 adolescentes, de entre 15 y 18 años de diferentes colegios, a quienes contagiaría con su entusiasmo y energía. Estos alumnos en su tiempo libre, fuera del ámbito escolar, colaboran con distintos hogares de niños y llevan donaciones a varias organizaciones sociales.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/300' }}
                                        />
                                        
                                    </Card>

                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>¿Sabías que…?</Title>
                                        <Paragraph style={styles.description}>
                                            las baterías de los celulares contienen metales pesados que contaminan el suelo si no son recicladas. En grandes cantidades, pueden matar animales y causar enfermedades a los seres humanos.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/400' }}
                                        />
                                     
                                    </Card>

                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>Es médica y atiende de forma gratuita a personas en situación de calle.</Title>
                                        <Paragraph style={styles.description}>
                                            Ella trabaja como médica clínica desde las siete de la mañana, durante más de 12 horas, de lunes a viernes. Al salir, se dirige a la Plaza Barrancas de Belgrano, donde un grupo de personas en situación de calle la espera. Por semana, asisten a más de 100 pacientes. Son los más vulnerables entre los vulnerables: desde familias que viven debajo de un puente, hasta personas que no tienen qué comer y gente con problemas de salud que jamás fueron tratados.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/500' }}
                                        />
                                     
                                    </Card>

                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>Un posteo en las redes que terminó ayudando a 30.000 personas.</Title>
                                        <Paragraph style={styles.description}>
                                            Publicó un mensaje en su perfil de Facebook al ver las desgarradoras imágenes de las inundaciones de La Plata: "Todos los que tengan donaciones para colaborar con los inundados y no puedan llevarlas, avísenme y yo las paso a buscar". Jamás imaginó lo que sucedería a partir de ese simple posteo. Llegaron más de 90 mensajes y llamados sin parar. Esto la hizo reflexionar. "Hay mucha energía solidaria desperdiciada. Todo el mundo quiere hacer algo para transformar la realidad y, por diferentes razones, no lo está haciendo", cuenta. Así se dio origen a una fundación que lleva más de 200 proyectos solidarios cumplidos, los cuales ya impactaron positivamente en 30.000 personas.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/600' }}
                                        />
                                     
                                    </Card>

                                    <Card style={styles.mainContainer}>
                                        <Card.Content />
                                        <Title style={styles.title}>Logró salir de situación de calle y hoy abriga y da de comer a los que están pasando frío.</Title>
                                        <Paragraph style={styles.description}>
                                            Marcelo solía pasar las noches en las calles de Almagro. Tenía un retraso emocional y madurativo, y era analfabeto. Actualmente, casi dos años después, el no solo va a la escuela y pudo salir de la calle, sino que forma parte de un equipo de voluntarios. Vive en un hotel, gracias a un subsidio habitacional, y se dedica a entregar frazadas o un plato caliente a los que menos tienen.
                                        </Paragraph>
                                        <Card.Cover
                                            style={styles.image}
                                            source={{ uri: 'https://picsum.photos/700' }}
                                        />
                                     
                                    </Card>
                            </View>
        )
    }
}

export default Noticias;

const styles = StyleSheet.create({
    mainContainer: {
        borderColor: '#DCEEFF',
        borderWidth: 1,
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 10,
    },
    image: {
        borderBottomEndRadius: 8,
        height: 140,
        marginBottom: 5,
        marginHorizontal: 5
        //padding: 10
    },
    description: {
        marginLeft: 15,
        marginBottom: 10,
        marginRight: 10,
        textAlign: 'justify',
        color: Colors.grey800,
    },
    title: {
        margin: 10,
        color: Colors.black,
    },
})