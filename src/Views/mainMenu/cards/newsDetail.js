import * as React from 'react';
import {
  Avatar,
  Button,
  Title,
  Paragraph,
  Colors,
  Caption,
  FAB
} from 'react-native-paper';
import { ScrollView, StyleSheet, Image, View,} from 'react-native';
//import { NAVIGATION } from '../../assets/constants';

const newsDetail = () => (
  <ScrollView style={styles.container}>
    <Title style={styles.title}> Lorem Ipsum </Title>
    <Image style={styles.image} source={{ uri: 'https://picsum.photos/700' }} />
    <Caption style= {styles.caption}>Fecha: 22/10/2019 - Hora:10:19 </Caption>
    <Paragraph style = {styles.paragraph}>
      Interdum sapien eros blandit sit. Blandit magna habitasse aliquam duis est
      sem, vel conubia in in sodales, wisi dignissim non bibendum nec. Aenean
      velit ut, purus justo rutrum conubia, at fringilla aliquam aliquam et
      iaculis vestibulum, ac nibh maecenas. Et sed morbi suscipit mi, gravida id
      sit, congue mi pellentesque donec pede, sed convallis diam per diam
      curabitur ac. Potenti vel non, ultrices ut quam est ac pharetra. Mi aenean
      sit, vestibulum turpis. Purus orci mi in tortor, quisque libero curabitur,
      vestibulum suscipit mauris ipsum platea quisque, non sed id sit, dolor
      morbi cum lacus magna id eu. Libero amet maecenas, integer libero
      ultricies erat orci pellentesque aenean, tristique cras viverra vel donec,
      vehicula in orci neque ac. Commodo gravida, dolor ultricies ullamcorper
      sed nam arcu aliquam. Facilisis lacus tellus, in diam ac felis justo in
      suspendisse, eu varius, vehicula at eu a ullamcorper litora
    </Paragraph>
    <FAB style={styles.fab}
      medium
      icon="shareAlt"
      onPress={() => console.log('Pressed')}
      />
  </ScrollView>
);

const styles = StyleSheet.create({
  caption: {
    marginTop:5,
    marginBottom:10,
    marginLeft: 10,

  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  fab : {
    position: 'absolute',
    right: 0,
    bottom: 0,
    marginRight: 15,
    backgroundColor: Colors.blue300,
  },
  image: {
    marginTop: 10,
    height: 250,
  },
  title: {
    margin: 15,
    textAlign:'center',
  },
  paragraph: {
    textAlign:'justify',
    marginLeft: 15,
    marginRight:10,
    marginBottom:35,
  }
});
export default newsDetail;
