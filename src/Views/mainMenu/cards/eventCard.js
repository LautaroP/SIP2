import * as React from 'react';
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  ProgressBar,
  Colors,
  DefaultTheme,
  Caption,
} from 'react-native-paper';
import { ScrollView, StyleSheet, View } from 'react-native';
//import { NAVIGATION } from '../../assets/constants';

const pg = 0.8;

const eventCard = () => (
  <Card style={styles.mainContainer}>
    <Card.Content />
    <Card.Cover
      style={styles.image}
      source={{
        uri:
          'http://3.bp.blogspot.com/-1WYN_xQMR5s/Tgki5JN_k5I/AAAAAAAAAEU/DMVy8PZDMZw/s1600/un%2Btecho.jpg',
      }}
    />
    <Title style={styles.title}>Jornada Techo</Title>
    <Paragraph style={styles.description}>
      Sumate el sabado 25/11 a la construccion de hogares para las familias
      Barrio Haras del Trujui!{' '}
    </Paragraph>
    <View style={styles.progressContainer}>
      <View style={styles.statusContainer}>
        <Caption style={styles.countdown}>Faltan 4 dias</Caption>
        <Caption style={styles.status}>{pg * 100}%</Caption>
      </View>

      <ProgressBar
        style={styles.progressBar}
        progress={pg}
        color={
          pg > 0 && pg < 0.3
            ? Colors.red800
            : pg < 0.6
            ? Colors.yellow800
            : Colors.green500
        }
      />
    </View>
    <Card.Actions theme={theme} style={styles.actionHolder}>
      <Button
        theme={theme}
        style={styles.button}
        onPress={() => {
          //this.props.navigation.navigate(NAVIGATION.INICIATIVA, { [NAVIGATION.PARAMS.INICIATIVA_SELECTED]: item })
        }}>
        DETALLE
      </Button>
      <Button
        theme={theme}
        style={styles.button}
        onPress={() => {
          //onPress={() => {this.shareHandler(item.titulo, item.descripcion)}}
        }}>
        COMPARTIR
      </Button>
    </Card.Actions>
  </Card>
);

const styles = StyleSheet.create({
  actionHolder: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {},
  description: {
    margin: 10,
    color: Colors.grey700,
  },
  mainContainer: {
    borderColor: '#eeeeee',
    borderWidth: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  image: {},
  progressBar: {
    margin: 10,
  },
  progressContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  status: {
    marginRight: 15,
    marginBottom: -5,
    color: Colors.grey600,
    alignSelf: 'flex-end',
  },
  statusContainer: {
    display: 'inline-flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  countdown: {
    marginLeft: 15,
    marginBottom: -5,
    color: Colors.grey600,
  },
  title: {
    marginTop: 15,
    marginBottom: 5,
    marginLeft: 10,
    color: Colors.black,
  },
});
const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.blue600,
    accent: Colors.blue200,
    text: Colors.blue400,
  },
};
export default eventCard;
