import * as React from 'react';
import {
  Button,
  Title,
  Paragraph,
  Colors,
  Caption,
  TextInput,
  DefaultTheme,
} from 'react-native-paper';
import { ScrollView, StyleSheet, Image, View, Picker } from 'react-native';
import { DatePicker } from 'react-native-datepicker';
import StepIndicator from 'react-native-step-indicator';

//import { NAVIGATION } from '../../assets/constants';

class createEventForm extends React.Component {
  state = {
    eventName: '',
    category: '',
    date: '',
    location: '',
  };
  render() {
    return (
      <ScrollView style={styles.container}>
        <Title style={styles.title}> Crear Iniciativa</Title>
        <StepIndicator
          style = {styles.stepIndicator}
          customStyles={stepIndicatorStyle}
         currentPosition={this.state.currentPosition}
         labels={stepsLabels}
         stepCount = {2}
        />
        <TextInput
          style={styles.textInput}
          theme={theme}
          mode="outlined"
          label="Titulo de Iniciativa"
          value={this.state.eventName}
          onChangeText={text => this.setState({ eventName: text })}
        />

        <TextInput
          style={styles.textInput}
          theme={theme}
          mode="outlined"
          label="Descripción"
          multiline="true"
          value={this.state.eventName}
          onChangeText={text => this.setState({ eventName: text })}
        />
        <Picker
          style={styles.picker}
          selectedValue={this.state.category}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ category: itemValue })
          }>
          <Picker.Item label="Categoria" value="" />
          <Picker.Item label="JavaScript" value="js" />
        </Picker>

        <Picker //REEMPLAZAR CON EL DATE PICKER
          style={styles.picker}
          selectedValue={this.state.category}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ category: itemValue })
          }>
          <Picker.Item label="Fecha de Finalización" value="java" />
          <Picker.Item label="JavaScript" value="js" />
        </Picker>

        <Picker
          style={styles.picker}
          selectedValue={this.state.category}
          onValueChange={(itemValue, itemIndex) =>
            this.setState({ category: itemValue })
          }>
          <Picker.Item label="Ubicación" value="" />
          <Picker.Item label="JavaScript" value="js" />
        </Picker>

        <View style = {styles.actionsContainer}>
          <Button style={styles.continueButton} mode="contained">
            Continuar
          </Button>
          <Button style={styles.cancelButton}
            theme={theme}
          >Volver</Button>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  actionsContainer:{
    display:'flex',
     flexDirection: 'column',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  cancelButton: {
    marginTop:20,
    bottom: 0,

  },
  continueButton: {
    margin:15,
    backgroundColor: Colors.blue400,
  },
  picker: {
    margin: 15,
    height: 40,
  },
  stepIndicator: {
    marginTop:30,
  },
  title: {
    textAlign: 'center',
    marginTop: 15,
    marginBottom:15,
    color: Colors.blue500
  },
  textInput: {
    margin: 15,
    height:40,
  },
});

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.blue600,
    accent: Colors.blue600,
  },
};

const stepsLabels = ["Datos Iniciativa","Archivos"];
const stepIndicatorStyle = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: Colors.green700,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: Colors.green300,
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: Colors.green300
}
export default createEventForm;
