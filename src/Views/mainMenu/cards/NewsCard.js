import * as React from 'react';
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  ProgressBar,
  Colors,
  Caption,
  DefaultTheme,
} from 'react-native-paper';
import { ScrollView, StyleSheet } from 'react-native';
//import { NAVIGATION } from '../../assets/constants';

const pg = 0.8;

const eventCard = () => (
  <Card style={styles.mainContainer}>
    <Card.Content />
    <Title style={styles.title}>Lorem Ipsum </Title>
    <Paragraph style={styles.description}>
    Lorem ipsum dolor sit amet, erat pede fermentum a, velit eget fusce aliquam sapien. Dignissim curabitur suspendisse quis, vel minima aliquam quis vitae pretium arcu, quisque vel in suspendisse penatibus, dictumst augue venenatis etiam placerat euismod. Ut fuga. Elit placerat. Sit phasellus id, nulla praesent ac scelerisque sed morbi tristique, nibh velit id orci, a at consectetuer tortor.
    </Paragraph>
    <Card.Cover
      style={styles.image}
      source={{ uri: 'https://picsum.photos/700' }}
    />
    <Caption style={styles.caption}>Fecha: 20/10/2019 - 10:19</Caption>
    <Card.Actions style={styles.actionHolder}>
      <Button
        theme = {theme}
        style={styles.button}
        onPress={() => {
          //this.props.navigation.navigate(NAVIGATION.INICIATIVA, { [NAVIGATION.PARAMS.INICIATIVA_SELECTED]: item })
        }}>
        DETALLE
      </Button>
      <Button
        theme = {theme}
        style={styles.button}
        onPress={() => {
          //onPress={() => {this.shareHandler(item.titulo, item.descripcion)}}
        }}>
        COMPARTIR
      </Button>
    </Card.Actions>
  </Card>
);

const styles = StyleSheet.create({
  actionHolder: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
  button: {
  },
  caption: {
    marginLeft: 10,
    marginTop:5,
    fontSize:10,
  },
  description: {
    marginLeft: 15,
    marginBottom:10,
    marginRight:10,
    textAlign:'justify',
    color: Colors.grey800,
  },
  mainContainer: {
    borderColor: '#eeeeee',
    borderWidth: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  image: {
    height: 140,
    marginBottom:-20,
  },
  title: {
    margin: 10,
    color: Colors.black,
  },
});
const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
        ...DefaultTheme.colors,
        primary: Colors.blue600,
        accent: Colors.blue200,
        text: Colors.blue400,
    },
};
export default NewsCard;
