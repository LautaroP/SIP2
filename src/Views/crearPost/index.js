import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, Text, ScrollView, StyleSheet, Image, Alert } from 'react-native'
import TextInput from '../../../components/textInput/textInputPaper'
//import ButtonApp from '../../../components/Button/button'
import ButtonApp from '../../../components/Button/buttonPaper'
import { appStyles } from '../../index.styles';
import { NAVIGATION } from '../../../assets/constants';
import DatePicker from 'react-native-datepicker';
import PostearActions from '../../redux/crearPost/action'
import NumericInput from "../../../components/numericInput/numericInputPaper"
import { createPost } from '../../api/createPost'

import SpinnerView from '../../../components/SpinnerView';
import GeneralActions from '../../redux/generalState/action'
import AuthActions from '../../redux/authState/action'
const mapStateToProps = (state) => {
    return {
        crearPostState: state.crearPostState,
        user: state.authState.userData
    };
}
class crearPost extends Component {
    state = {
        titulo: '',
        descripcion: '',
        fecha: undefined,
        localizacion: 'localizacion',
        categoria: 'Problema Social',
        objetivoPersonas: "",
        objetivoMonto: "",
        errorTitulo: false,
        errorFecha: false,
        errorCategoria: false,
        errorPersonas: false,
        errorMonto: false,
    }
    attemptPost = () => {
        if (this.props.user) {
            const { titulo, localizacion: geoLocalizacion, objetivoPersonas, objetivoMonto, fecha: fechaLimite, categoria, descripcion } = this.state;
            this.props.setLoading(true)
            createPost(titulo, geoLocalizacion, objetivoPersonas, objetivoMonto, fechaLimite, categoria, descripcion, this.props.user.email).then(([success]) => {
                if (success) {
                    Alert.alert("Iniciativa Creada")
                    this.setState({
                        titulo: '',
                        descripcion: '',
                        fecha: undefined,
                        localizacion: '',
                        categoria: 'Problema Social',
                        objetivoPersonas: "",
                        objetivoMonto: "",
                        errorTitulo: false,
                        errorFecha: false,
                        errorCategoria: false,
                        errorPersonas: false,
                        errorMonto: false,
                    })
                    this.props.reLogin()
                    this.props.navigation.goBack()
                } else
                    Alert.alert("Error al crear Iniciativa")
                this.props.setLoading(false)
            })
        }
        else
            this.props.navigation.toggleDrawer()

    }
    componentWillMount() {
        if (!this.props.user) {
            this.props.navigation.navigate(NAVIGATION.MAIN_MENU)
            this.props.navigation.toggleDrawer()
        }
    }
    componentDidMount() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', () => {
            if (!this.props.user) {
                this.props.navigation.navigate(NAVIGATION.MAIN_MENU)
                this.props.navigation.toggleDrawer()
            }
            this.setState({
                titulo: '',
                descripcion: '',
                fecha: undefined,
                localizacion: 'localizacion',
                categoria: 'Problema Social',
                objetivoPersonas: "",
                objetivoMonto: "",
                errorTitulo: false,
                errorFecha: false,
                errorCategoria: false,
                errorPersonas: false,
                errorMonto: false,
            })
        });
    }
    data = [
        { id: 1, title: "Social", value: "Problema Social", color: "#87CEEB", image: "https://img.icons8.com/plasticine/100/000000/family.png" },
        { id: 2, title: "Ambiental", value: "Problema Ambiental", color: "#4682B4", image: "https://img.icons8.com/dusk/70/000000/globe-earth.png" },
        { id: 3, title: "Salud", value: "Salud", color: "#6A5ACD", image: "https://img.icons8.com/plasticine/100/000000/nurse-female.png" },
        { id: 4, title: "Económico", value: "Economico", color: "#20B2AA", image: "https://img.icons8.com/plasticine/100/000000/cash-.png" },
    ]
    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }
    verifyPaso1 = () => {
        const { titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto } = this.state
        let flag = true;
        if (!titulo) {
            this.setState({ errorTitulo: true })
            flag = false
        }
        if (!fecha) {
            this.setState({ errorFecha: true })
            flag = false
        }
        if (!objetivoMonto && !objetivoPersonas) {
            this.setState({ errorMonto: true, errorPersonas: true })
            flag = false
        }
        if (!categoria) {
            this.setState({ errorCategoria: true })
            flag = false
        }
        return flag
    }
    attemptGoToAddFiles = () => {
        const { titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto } = this.state
        if (this.verifyPaso1())
            this.attemptPost()
        else
            Alert.alert("Datos Incorrectos", "Complete todos los campos obligatorios")
    }

    render() {
        const { titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto, errorCategoria, errorFecha, errorMonto, errorPersonas, errorTitulo } = this.state
        return (
            <SpinnerView style={styles.container}>
                <View style={{ width: "98%", height: "90%", alignContent: 'center', justifyContent: 'space-around' }}>
                    <ScrollView style={{ width: "98%", height: "90%" }}>
                        <View style={{ alignContent: 'center', width: '90%', alignSelf: 'center' }} behavior="padding" enabled>
                            <View style={{ marginTop: 15, flex: 1 }}>
                                <TextInput style={{ margintop: 25, flex: 1, marginBottom: 20 }} error={errorTitulo}
                                    label={"Título"} onChangeText={(text) => this.setState({ titulo: text, errorTitulo: false })} value={titulo} />
                                {/* <ComboBox style={{ flex: 1, marginTop: -40 }}
                                onChangeText={(categoria) =>
                                    this.setState({ categoria: categoria, errorCategoria: false })
                                }
                                dropdownStyle={{ color: errorCategoria ? 'red' : undefined }}
                                // data={this.props.crearPostState.categorias.map((value) => { { value: value } })}
                                data={this.data}
                                value={"Categoría"}
                                onch /> */}
                                <TextInput style={{ flex: 1, marginBottom: 20 }}
                                    label={"Descripción"} optionalBool onChangeText={(text) => this.setState({ descripcion: text })} value={descripcion} />
                                <View style={{ flex: 1, width: '100%', marginBottom: 15 }}>
                                    <DatePicker style={{ flex: 1, width: '100%', color: errorFecha && 'red' }} placeholder="Fecha Límite"
                                        mode="date"
                                        format="DD/MM/YYYY"

                                        minDate={new Date().getDate()}
                                        date={fecha} onDateChange={(date) => { this.setState({ fecha: date, errorFecha: false }) }} />
                                </View>
                                {/* <TextInput style={{ flex: 1, marginBottom: 20 }}
                                optionalBool label={"Localización"} onChangeText={(text) => this.setState({ localizacion: text })} value={localizacion} /> */}
                                <View style={{ width: "100%", justifyContent: "space-between", flexDirection: 'row', marginBottom: 20, flex: 1 }}>
                                    <NumericInput style={{ flex: this.state.objetivoPersonas ? 2 : 1, marginRight: 10 }} error={errorPersonas}
                                        label={"Personas requeridas"} onChangeText={(objPpl) => this.setState({ objetivoPersonas: objPpl, objetivoMonto: '', errorMonto: false, errorPersonas: false })} value={objetivoPersonas} />
                                    <Text style={{ alignSelf: 'center', marginRight: 10 }}>ó</Text>
                                    <NumericInput style={{ flex: this.state.objetivoMonto ? 2 : 1, color: 'light-grey' }} error={errorMonto}
                                        label={"Monto requerido"} onChangeText={(objMoney) => this.setState({ objetivoMonto: objMoney, objetivoPersonas: '', errorMonto: false, errorPersonas: false })} value={objetivoMonto} />
                                </View>
                                <View style={{ width: '100%', flexDirection: 'row', flex: 1, marginBottom: 20 }}>
                                    {this.data.map(item => (<TouchableOpacity style={{ flex: 1, backgroundColor: item.value == categoria ? item.color : 'grey', borderRadius: 5, borderStyle: 'solid', borderWidth: 1, marginHorizontal: 1.5 }} onPress={() => this.setState({ categoria: item.value })}>
                                        <Text numberOfLines={1} style={styles.title}>{item.title}</Text>
                                        <Image style={styles.cardImage} source={{ uri: item.image }} />
                                    </TouchableOpacity>))}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={{ alignSelf: 'center' , justifyContent: 'center', flexDirection: 'row'}}>
                         <ButtonApp label={"Crear Iniciativa"} onPress={this.attemptGoToAddFiles} style={{ flex: 1, marginHorizontal: 30 }} />
                    </View>
                </View>
            </SpinnerView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 5,
        backgroundColor: '#f8f8f8'
    },
    list: {
        //paddingHorizontal: 5,
        backgroundColor: "white",
    },
    listContainer: {
        alignItems: 'center'
    },
    /******** card **************/
    card: {
        marginHorizontal: 2,
        marginVertical: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        borderWidth: 1,
    },
    cardHeader: {
        paddingVertical: 17,
        paddingHorizontal: 16,
        borderTopLeftRadius: 1,
        borderTopRightRadius: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: "center"
    },
    cardContent: {
        paddingVertical: 12.5,
        paddingHorizontal: 16,
    },
    cardImage: {
        height: 70,
        width: 70,
        alignSelf: 'center'
    },
    title: {
        fontSize: 14,
        flex: 1,
        color: "#FFFFFF",
        fontWeight: 'bold',
        textAlign: 'center',
    },
    subTitle: {
        fontSize: 12,
        flex: 1,
        color: "#FFFFFF",
    },
    icon: {
        height: 20,
        width: 20,
    }
})

const mapDispatchToProps = (dispatch) => {
    return {
        setLoading: (loading) => dispatch(GeneralActions.setLoading(loading)),
        reLogin: () => dispatch(AuthActions.reLogin()),
        attemptSetPaso1: (titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto, callback) => dispatch(PostearActions.attemptSetPaso1(titulo, descripcion, fecha, localizacion, categoria, objetivoPersonas, objetivoMonto, callback))
    }
};
export default connect(
    mapStateToProps, mapDispatchToProps
)(crearPost);