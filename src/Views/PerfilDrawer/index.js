import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, KeyboardAvoidingView, Text, StyleSheet } from 'react-native'
import TextInput from '../../../components/textInput/textInput'
//import Button from '../../../components/Button/button'
import ButtonApp from '../../../components/Button/buttonPaper'
import { appStyles, colors } from '../../index.styles';
import LoginActions from '../../redux/authState/action'
import PasswordInput from '../../../components/passwordInput/passwordInput';
import SpinnerView from '../../../components/SpinnerView';
import { NAVIGATION } from '../../../assets/constants';
import LinkButton from '../../../components/linkButton/linkButton';
import { EvilIcons } from '@expo/vector-icons';
const mapStateToProps = (state) => {
    return {
        user: state.authState.userData
    };
}
class PerfilDrawer extends Component {

    tryLogout = () => {
        this.props.attemptLogOut()
        this.props.navigation.navigate(NAVIGATION.MAIN_MENU)
    }
    render() {
        const user = this.props.user
        return (
            <SpinnerView>
                <EvilIcons style={{ color: colors.APP_BLUE, alignSelf: 'center', marginTop: 20 }} name={'user'} size={150} />

                <Text style={styles.name}>{user.nombre + ' ' + user.apellido}</Text>
                <Text style={styles.info}>{user.email}</Text>

                <View style={styles.profileDetail}>
                    <View style={styles.detailContent}>
                        <Text style={styles.title}>Iniciativas</Text>
                        <Text style={styles.count}>{user.impulsadas ? user.impulsadas.length : 0}</Text>
                    </View>
                    <View style={styles.detailContent}>
                        <Text style={styles.title}>Colaboraciones</Text>
                        <Text style={styles.count}>{user.colaboraciones ? user.colaboraciones.length : 0}</Text>
                    </View>
                </View>
                <View style={styles.button}>
                    <ButtonApp
                    style = {{ marginTop:10, flex:1}} 
                    label={"Desconectarse"} onPress={this.tryLogout} />
                </View>
                
                {/* <View style={[appStyles.horizontalView, { alignItems: 'center', justifyContent: 'center', marginTop: 10 }]}>
                    <View style={[{ width: '50%', alignItems: 'flex-end', paddingRight: 30 }]}>
                        <LinkButton onPress={this.trySignin} >Desconectarse</LinkButton>
                    </View>
                    <View style={[{ width: '50%', alignItems: 'flex-start', paddingLeft: 30 }]}>
                        <LinkButton onPress={this.trySignin} >{'Olvidé mi\n contraseña'}</LinkButton>
                    </View>
                </View> */}
            </SpinnerView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        attemptLogOut: () => dispatch(LoginActions.logout())
    }
};
export default connect(
    mapStateToProps, mapDispatchToProps
)(PerfilDrawer);


const styles = StyleSheet.create({
    header: {
        backgroundColor: '#2099FB',
        height: 200,
    },
    avatar: {
        width: 130,
        height: 130,
        borderRadius: 63,
        borderWidth: 6,
        borderColor: "white",
        marginBottom: 10,
        alignSelf: 'center',
        position: 'absolute',
        marginTop: 100,
        zIndex: 1,
    },
    body: {
        position: 'absolute',
        alignSelf: 'center',
        width: 350,
        borderRadius: 38,
        marginTop: 170,
        paddingTop: 30,
        zIndex: 0,
        backgroundColor: '#F9F9F9',
    },
    bodyContent: {
        backgroundColor: 'white',
        flex: 1,
        alignItems: 'center',
        padding: 30,
        marginBottom: 100,
        backgroundColor: '#F9F9F9',
    },
    name: {
        alignSelf: 'center',
        fontSize: 28,
        color: "#696969",
        fontWeight: "600"
    },
    info: {
        alignSelf: 'center',
        fontSize: 16,
        color: "#2099FB",
        marginTop: 10
    },
    opcions: {
        fontSize: 16,
        color: "black",
    },
    description: {
        fontSize: 16,
        color: "#696969",
        marginTop: 10,
        textAlign: 'center'
    },
    icon: {
        width: 30,
        height: 30,
        marginTop: 2,
        marginRight: 10,
    },
    touchable: {
        marginTop: 25,
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 250,
        borderRadius: 30,
        backgroundColor: "white",
    },
    profileDetail: {
        marginVertical: 10,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'lightgrey',
        borderRadius: 30,
    },
    detailContent: {
        margin: 10,
        alignItems: 'center',
    },
    title: {
        fontSize: 16,
        color: "black",
    },
    count: {
        fontSize: 18,
        color: '#2099FB',
    },
    button: {
        alignSelf: "center",
        flexDirection:'row'
    },
});