import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, KeyboardAvoidingView, Text, TouchableOpacity, Alert } from 'react-native'
import ButtonApp from '../../../components/Button/button'
import { appStyles, colors } from '../../index.styles';
import { NAVIGATION } from '../../../assets/constants';
import { createPost } from '../../api/createPost'
import { FontAwesome } from '@expo/vector-icons'
import { Button, Snackbar } from 'react-native-paper';

import SpinnerView from '../../../components/SpinnerView';
import GeneralActions from '../../redux/generalState/action'

const mapStateToProps = (state) => {
    return {
        crearPostState: state.crearPostState,
        user: state.authState.userData,
    };
}
class addFiles extends Component {
    state = {
        fecha: "",
        titulo: '',
        descripcion: '',
        localizacion: '',
        objetivoPersonas: "",
        objetivoMonto: "",
        visible:false,
    }
    attemptPost = () => {
        if (this.props.user) {
            const { titulo, localizacion: geoLocalizacion, objetivoPersonas, objetivoMonto, fecha: fechaLimite, categoria, descripcion } = this.props.crearPostState;
            this.props.setLoading(true)
            createPost(titulo, geoLocalizacion, objetivoPersonas, objetivoMonto, fechaLimite, categoria, descripcion, this.props.user.email).then(([success]) => {
                if (success) {
                    this.props.navigation.dangerouslyGetParent().navigate(NAVIGATION.CREATE_POST)
                    this.setState({ visible: true })
                } else
                    Alert.alert("Error al crear Post")
                this.props.setLoading(false)
            })
        }
        else
            this.props.navigation.toggleDrawer()

    }
    render() {
        let localizacionInput, titleInput, descripcionInput, categoriaInput, fechaInput;
        return (
            <SpinnerView style={{ flex: 1, alignContent: 'center', width: '90%', alignSelf: 'center', alignItems: 'center' }} >
                <Text style={appStyles.textTituloGrande}>Paso 2/2</Text>
                <Text style={appStyles.textTitulo}>Agregar Multimedia</Text>
                {/* <TouchableOpacity>
                    <FontAwesome name={"folder"} color={colors.APP_LIGHT_BLUE} size={40} />
                </TouchableOpacity> */}
                <TouchableOpacity>
                    <FontAwesome name={"folder-o"} color={colors.APP_LIGHT_BLUE} size={250} />
                </TouchableOpacity>
                <ButtonApp label={"Agregar Archivo"} onPress={this.addFile} />
                <View style={{ flex: 1, alignSelf: 'center', alignItems: 'center',justifyContent:'flex-end',marginBottom:20 }}>
                    <ButtonApp label={"Crear Publicación"} big={true} onPress={this.attemptPost} />
                </View>
            </SpinnerView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setLoading: (loading) => dispatch(GeneralActions.setLoading(loading)),
    }
};
export default connect(
    mapStateToProps, mapDispatchToProps
)(addFiles);