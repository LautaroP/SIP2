import React, { Component } from 'react';
import { Platform, StyleSheet, View, ScrollView, Text, ActivityIndicator } from 'react-native';
import { ProgressBar, Colors, Avatar, Button, Card, Title, Paragraph, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { NAVIGATION } from '../../../assets/constants';
import { appStyles } from '../../index.styles';
import IniciativasList from '../../../components/iIniciativasList/iniciativasList';
import { connect } from 'react-redux';
import GeneralActions from '../../redux/generalState/action'


class ResultadoFiltro extends Component {

  state = {
    initiativeExistence: null,
    categoria: '',
    iniciativas: '',
  }

  componentDidMount() {
    const categoria = this.props.navigation.getParam(NAVIGATION.PARAMS.CATEGORIA, undefined);
    this.setState({ categoria: categoria });
    let requestBody = {};
    requestBody.categoria = categoria
    this.props.setLoading(true)

    fetch('https://still-beach-77233.herokuapp.com/rutes/getIniciativasByCategoria', {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: new Headers({
        'Content-Type': 'application/json'
      }),
    })
      .then(response => {
        if (response.status === 200) {
          this.setState({ initiativeExistence: true });
          return response.json();
        }
        else {
          this.setState({ initiativeExistence: false });
          return response.json();
        }
      })
      .then(
        (result) => {
          this.setState({ iniciativas: result });
          this.props.setLoading(false)
        }
      )
  };



  render() {
    const { initiativeExistence, categoria, iniciativas } = this.state;
    return (

      <IniciativasList navigation={this.props.navigation} data={iniciativas} dataExists={initiativeExistence} categoria={categoria} />
    );
  }
};

const mapStateToProps = (state) => {
  return {
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLoading: (loading) => dispatch(GeneralActions.setLoading(loading))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultadoFiltro)