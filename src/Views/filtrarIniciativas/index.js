import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Dimensions,
  Platform,
  StatusBar,
  TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { NAVIGATION } from '../../../assets/constants';
import secureFetch from '../../api/secureFetch';
import SearchHeader from '../../../components/SearchHeader'

export default class FiltrarIniciativas extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [
        { id: 1, title: "Problema social", value: "Problema Social", color: "#87CEEB", image: "https://img.icons8.com/plasticine/100/000000/family.png" },
        { id: 2, title: "Problema ambiental", value: "Problema Ambiental", color: "#4682B4", image: "https://img.icons8.com/dusk/70/000000/globe-earth.png" },
        { id: 3, title: "Salud", value: "Salud", color: "#6A5ACD", image: "https://img.icons8.com/plasticine/100/000000/nurse-female.png" },
        { id: 4, title: "Económico", value: "Economico", color: "#20B2AA", image: "https://img.icons8.com/plasticine/100/000000/cash-.png" },
      ],

      textSearch: '',
      seriesList:'',
    };
  }

  componentWillMount() {
    this.startHeaderHeight = 80
    if (Platform.OS == 'android') {
        this.startHeaderHeight = 70 + StatusBar.currentHeight
    }
}

onSearch = (searchInput) => {
  if (!(!searchInput || /^\s*$/.test(searchInput))) { 
          this.props.navigation.navigate(NAVIGATION.FILTER_RESTULTS_PALABRA, { [NAVIGATION.PARAMS.TITULO]: searchInput })}
    }


  render() {
    console.log(this.state)
    return (
      <View style={styles.container}>

          <View style={styles.searchContainer}>
            <View style={styles.searchView}>
              <SearchHeader 
                action = {this.onSearch}/>
            </View>
          </View>

        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={this.state.data}
          horizontal={false}
          numColumns={2}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => {
            return (
              <TouchableOpacity style={[styles.card, { backgroundColor: item.color }]} onPress={() => this.props.navigation.navigate(NAVIGATION.FILTER_RESTULTS, { categoria: item.value })}>
                <View style={styles.cardHeader}>
                  <Text style={styles.title}>{item.title}</Text>
                </View>
                <Image style={styles.cardImage} source={{ uri: item.image }} />
              </TouchableOpacity>
            )
          }} />
      </View>
    );
  }
}

var { height, width } = Dimensions.get('window');
var box_count = 2;
var box_width = (width / box_count) * 0.9;
var box_height = height * 0.33

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: '#f8f8f8'
  },
  list: {
    //paddingHorizontal: 5,
    marginTop: 10,
    backgroundColor: "white",
  },
  listContainer: {
    alignItems: 'center'
  },
  /******** card **************/
  card: {
    marginVertical: 2,
    marginHorizontal:5,
    flexBasis: box_width,
    height: box_height,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderWidth: 1,
  },
  cardHeader: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "center"
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
  },
  cardImage: {
    height: 70,
    width: 70,
    alignSelf: 'center'
  },
  title: {
    fontSize: 16,
    flex: 1,
    color: "#FFFFFF",
    fontWeight: 'bold',
    textAlign: 'center',
  },
  subTitle: {
    fontSize: 12,
    flex: 1,
    color: "#FFFFFF",
  },
  icon: {
    height: 20,
    width: 20,
  },
  searchContainer: {
    /*height: this.startHeaderHeight,
    backgroundColor: 'white',
    borderBottomWidth: 1,*/
    height: 65,
    justifyContent: 'center'
  },
  searchView: {
    flexDirection: 'row',
    //padding: 10,
    marginHorizontal: 15,
    //shadowOffset: { width: 0, height: 0 },
    //shadowColor: 'grey',
    //shadowOpacity: 0.2,
    //elevation: 1,
    backgroundColor: '#f8f8f8',
    marginTop: Platform.OS == 'android' ? 30 : null,
    paddingBottom: Platform.OS == 'android' ? 30 : null,
  },
});   