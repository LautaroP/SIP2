import React, { Component } from 'react';
import { Platform, StyleSheet, View, ScrollView, Text, ActivityIndicator } from 'react-native';
import { ProgressBar, Colors, Avatar, Button, Card, Title, Paragraph, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { NAVIGATION } from '../../../assets/constants';
import { appStyles } from '../../index.styles';
import IniciativasList from '../../../components/iIniciativasList/iniciativasList';
import { connect } from 'react-redux';
import GeneralActions from '../../redux/generalState/action'


class ResultadoFiltro extends Component {

  state = {
    initiativeExistence: null,

    iniciativas: [],
  }

  componentDidMount() {
    const titulo = this.props.navigation.getParam(NAVIGATION.PARAMS.TITULO, undefined);
    this.props.setLoading(true)

    fetch('https://still-beach-77233.herokuapp.com/rutes/getIniciativasByWord/' + titulo)
      .then(response => {
        if (response.status === 200) {
          this.setState({ initiativeExistence: true });
          console.log(response.status);
          
          return response.json();
        }
        else {
          if (response.status === 404) {
            this.setState({ initiativeExistence: false });
            return response.json();
          }
          else {
            this.setState({ initiativeExistence: false });
            return response.text();
          }
        }
      })
      .then(
        (result) => {
          this.setState({ iniciativas: result });
          this.props.setLoading(false)
        }
      )
  };



  render() {
    const { initiativeExistence, iniciativas } = this.state;
    return (

      <IniciativasList navigation={this.props.navigation} data={iniciativas} dataExists={initiativeExistence} />
    );
  }
};

const mapStateToProps = (state) => {
  return {
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setLoading: (loading) => dispatch(GeneralActions.setLoading(loading))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultadoFiltro)