import React from "react";
import { View, StyleSheet } from "react-native";
import { appStyles, colors } from "../../src/index.styles";
import { TextInput } from "react-native-paper"

class TextInputPaper extends React.Component {

  render() {
    //console.log(this.props);
    const style = this.props.style

    return (
      <TextInput
        style={[styles.textInput, style]}
        theme={theme}
        mode="outlined"
        selectionColor="#2099FB"
        underlineColor="#2099FB"
        label={this.props.label} //La idea es que en otherProps se mande lo de label
        onChangeText={this.props.onChangeText}
        value={this.props.value}
        inlineImageLeft={this.props.inlineImageLeft}
        inlineImagePadding={this.props.inlineImagePadding}
      //value={this.state.eventName}
      //onChangeText={text => this.setState({ eventName: text })}
      />
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    // margin: 15,
    //height:40,
    //flex: 1,
    width: '100%'
  },
})

const theme = {
  //...DefaultTheme,
  roundness: 10,
  colors: {
    //...DefaultTheme.colors,
    primary: colors.APP_BLUE,
    accent: colors.APP_BLUE_HOVER,
  },
};

export default TextInputPaper;

