import React from "react";
import { TextInput, View, Text } from "react-native";
import { appStyles, colors } from "../../src/index.styles";
import ReinputInput from "reinput";

class FormTextInput extends React.Component {
  textInputRef;
  isFocused() {
    if (this.textInputRef)
      this.textInputRef.register.isFocused();
  }
  onFocus(callback) {
    if (this.textInputRef)
      this.textInputRef.register.onFocus(callback);
  }
  render() {
    const { style, error, optionalBool, label, ref, ...otherProps } = this.props;
    return (
      <ReinputInput
        ref={(input) => this.textInputRef = input}
        fontSize={16}
        style={[style]}
        // underlineHeight={0.5}
        // underlineActiveHeight={1}
        underlineColor={colors.APP_LIGHT_BLUE}
        labelActiveColor={colors.APP_LIGHT_BLUE}
        underlineActiveColor={error ? 'red' : colors.APP_BLUE}
        labelColor={error ? 'red' : colors.APP_LIGHT_BLUE}
        // register={ref}
        {...otherProps}
        label={optionalBool ? this.props.label + " (opcional)" : this.props.label}
      />
    );
  }
}

export default FormTextInput;