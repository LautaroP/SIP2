import React from 'react';
import { Text, TouchableHighlight, Image, View, TouchableOpacity, StyleSheet } from 'react-native';
import { appStyles, colors } from "../../src/index.styles";
import { Button } from 'react-native-paper';

class ButtonApp extends React.Component {
  render() {
    const { label, onPress, style, disabled, onPressDisabled, children, textStyle, big, ...otherProps } = this.props;
    return (

      <TouchableOpacity
        mode="outlined"
        style={[styles.touchable, style]}
        onPress={!disabled ? onPress : onPressDisabled}
        {...otherProps}
      >
        <Text style={[{ fontSize: big ? 30 : 20, color: colors.APP_BLUE, marginHorizontal: 10 }, textStyle]}>
          {children || label}
        </Text>
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  touchable: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: colors.APP_BLUE,
    backgroundColor: colors.APP_LIGHT_GREY_COLOR,
    marginVertical: 7,
    padding: 3,
  },
})

export default ButtonApp;