import React from 'react';
import { Text, TouchableHighlight, Image, View, TouchableOpacity, StyleSheet } from 'react-native';
import { appStyles, colors } from "../../src/index.styles";
import { Button } from 'react-native-paper';

class ButtonPaper extends React.Component {
  render() {
      //console.log(this.props);
      
    if (this.props.style) {
        styles.propsStyle = this.props.style
    }

    return (
        <Button
            style={[styles.buttons, styles.propsStyle]}
            color={colors.APP_BLUE}
            mode="outlined"
            onPress={this.props.onPress}
            //{...otherProps} //La idea aca es que el onPress venga en otherProps
            >{this.props.label}
        </Button>
    );
  }
}
const styles = StyleSheet.create({
    buttons: {
        borderColor: "#2099FB",
        borderRadius: 10,
        borderWidth: 2,
        width: 115
    },
    propsStyle: {}
})

export default ButtonPaper