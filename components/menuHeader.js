import React from 'react';
import { View, Text, Image, SafeAreaView, Platform, StatusBar, TouchableOpacity } from 'react-native';
import { Header } from 'react-native-elements';
import { FontAwesome, Feather, EvilIcons } from "@expo/vector-icons";
import { Colors } from 'react-native-paper';

export default class MenuHeader extends React.Component {

    handleOnGoBack(onGoBack, navigation) {
        if (onGoBack)
            onGoBack(navigation);
        else
            navigation.goBack(null);
    }

    render() {
        const { style, haveBack, navigation, title, logo, ...otherProps } = this.props;
        let onGoBack = navigation.getParam("onGoBack");
        return (

            <SafeAreaView>
                <Header
                    containerStyle={Platform.OS === 'ios' ? { paddingTop: 0, height: 40 } : { paddingTop: StatusBar.currentHeight, height: 40 + StatusBar.currentHeight, backgroundColor:Colors.white, borderBottomWidth:1, borderBottomColor:Colors.grey200, zIndex:10,}}
                    centerComponent={
                        <View style={{ alignItems: 'center', flexDirection: 'row', alignSelf: 'center', alignContent: 'center', justifyContent: 'center' }}>
                            {/*<Text style={{ color: 'white', fontWeight: 'bold', fontSize: 18 }}>{title}</Text>*/}
                            <Image style={{height: 80, width: 150}} source={logo}/>
                        </View>
                    }
                    leftComponent={haveBack ? (
                        <TouchableOpacity onPress={() => { this.handleOnGoBack(onGoBack, navigation) }} style={{ width: 30 }}>
                            <FontAwesome name='angle-left' size={40} style={{ color: '#16B9D9' }} />
                        </TouchableOpacity>
                    ) : (
                            <TouchableOpacity onPress={() => { navigation.toggleDrawer() }} >
                                <EvilIcons style={{ color: '#16B9D9' }} name={'navicon'} size={40} />
                            </TouchableOpacity>
                        )}
                />
            </SafeAreaView>
        );
    }
}