import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ActivityIndicator } from 'react-native'
import { appStyles } from '../../src/index.styles';

const mapStateToProps = (state) => {
    return {
        generalState: state.generalState
    };
}
class SpinnerView extends Component {

    render() {
        let { generalState, style, loading, ...otherProps } = this.props;
        loading = loading || generalState.loading
        return loading ? (
            <View style={{ flex: 1, height: '100%', alignItems: 'center', alignContent: "center" }}>
                <ActivityIndicator style={{ flex: 1, alignSelf: 'center' }} size={70} />
            </View>) :
            <View style={[appStyles.SpinnerView, style]}{...otherProps} />


    }
}


export default connect(
    mapStateToProps
)(SpinnerView);