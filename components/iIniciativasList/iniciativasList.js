import React, { Component } from 'react';
import { Platform, StyleSheet, View, ScrollView, Text, ActivityIndicator, Share, TouchableOpacity } from 'react-native';
//import { ProgressBar, Colors, Avatar, Card, Title, Paragraph, DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { NAVIGATION } from '../../assets/constants';
import { appStyles, colors } from '../../src/index.styles';
import SpinnerView from '../SpinnerView/index'
import {
    Provider as PaperProvider,
    Button,
    Card,
    Title,
    Paragraph,
    ProgressBar,
    Colors,
    DefaultTheme,
    Caption,
} from 'react-native-paper';

export default class IniciativasList extends Component {
    random() {
        var array = ['https://picsum.photos/100', 'https://picsum.photos/200', 'https://picsum.photos/300', 'https://picsum.photos/400', 'https://picsum.photos/500', 'https://picsum.photos/600', 'https://picsum.photos/700', 'https://picsum.photos/800', 'https://picsum.photos/900']
        return array[Math.floor(Math.random() * (8))]
    }
    shareHandler(title, description) {
        //console.log(title);
        var link = "\nPara conocer como ayudar, descargate HelpUp! y busca la iniciativa: '" + title + "'. Esperamos verte pronto con nosotros!"
        Share.share({
            //title: title, --> No estaria mostrando nada
            message: description + link,
        })
    }

    dateDiff(origDate) {
        var diff = new Date(origDate).getTime() - new Date().getTime();    //fecha limite - fecha actual
        var dias = Math.floor(diff / (1000 * 60 * 60 * 24));
        dias = (dias * -1)
        //console.log(dias);

        return (dias * -1)
    }

    eventPerc(event) {
        var acumPer = event.objetivoPersonaAcumulado
        var espPer = event.objetivoPersonaEsperado
        var acumMonto = event.objetivoEconomicoAcumulado
        var espMonto = event.objetivoEconomicoEsperado
        var perc = 0 

        if (acumPer && espPer) {
            perc = acumPer / espPer
            //return (Math.round(perc * 10) / 10)
        } else {
            if (acumMonto && espMonto) {
                perc = acumMonto / espMonto
                //return (Math.round(perc * 100) / 100)
            }else {
                perc = 0
                //return (Math.round(perc * 100) / 100)
            }
        }
        if (perc > 0.04 ) {
            perc = (Math.round(perc * 10) / 10)
        } else {
            perc = (Math.round(perc * 100) / 100)
        }
        
        //console.log("fixed: " + perc);
            
            return perc

    }



    render() {
        const { dataExists, categoria, data } = this.props;

        return (
            <SpinnerView>

                <PaperProvider theme={theme} style={{backgroundColor: '#f8f8f8'}}>
                    {
                        !dataExists
                            ? //<ActivityIndicator size="large" color="#0000ff" style={{ flex: 10 }} />
                            <View style={styles.empty}>
                                <Text style={appStyles.textTituloGrande}>No hay iniciativas</Text>
                            </View>
                            : (data && data.length > 0)/* && iniciativas.length > 0*/ ?
                                <ScrollView style={{ backgroundColor: '#f8f8f8'}}>
                                    <View style={styles.container}>
                                        {
                                            (data && data.length > 0 ? data : []).map(item => (
                                                <TouchableOpacity
                                                style={{ backgroundColor: '#f8f8f8'}}
                                                    onPress={() => this.props.navigation.navigate(NAVIGATION.INICIATIVA, { [NAVIGATION.PARAMS.INICIATIVA_SELECTED]: item })}>

                                                    <Card style={styles.mainContainer}>
                                                        <Card.Content />
                                                        <Card.Cover
                                                            style={styles.image}
                                                            source={{ uri: this.random() }}
                                                        />
                                                        <Title style={styles.title}>{item.titulo}</Title>
                                                        {
                                                            //<Card.Title title={item.titulo} left={(props) => <Avatar.Icon {...props} icon="label-outline" />} />
                                                        }
                                                        <Paragraph style={styles.description}>
                                                            {item.descripcion}
                                                        </Paragraph>
                                                        {
                                                            // <Card.Content>
                                                            //     <Paragraph>{item.descripcion}</Paragraph>
                                                            //     <ProgressBar progress={item.progress} color={Colors.blue800} />
                                                            // </Card.Content>
                                                        }
                                                        <View style={styles.progressContainer}>
                                                            <View style={styles.statusContainer}>
                                                                {
                                                                    item.fechaLimite
                                                                    ? <Caption style={styles.countdown}>
                                                                        Faltan {this.dateDiff(item.fechaLimite)} dias
                                                                      </Caption>
                                                                    :<Caption></Caption>
                                                                }
                                                                
                                                                <Caption style={styles.status}>
                                                                    Objetivo: {this.eventPerc(item) * 100}%
                                                            </Caption>
                                                            </View>

                                                            <ProgressBar
                                                                style={styles.progressBar}
                                                                progress={this.eventPerc(item)}
                                                                color={
                                                                    this.eventPerc(item) > 0 && this.eventPerc(item) < 0.3
                                                                        ? Colors.red800
                                                                        : this.eventPerc(item) < 0.6
                                                                            ? Colors.yellow800
                                                                            : Colors.green500
                                                                }
                                                            />
                                                        </View>
                                                        {
                                                            //<Card.Cover style={styles.image} source={{ uri: this.random() }} />
                                                        }
                                                        <Card.Actions theme={theme} style={styles.actionHolder}>
                                                            <Button
                                                                style={styles.buttons}
                                                                color={colors.APP_BLUE}
                                                                mode="outlined"
                                                                onPress={() => this.props.navigation.navigate(NAVIGATION.INICIATIVA, { [NAVIGATION.PARAMS.INICIATIVA_SELECTED]: item })}

                                                            >Detalles</Button>
                                                            <Button
                                                                style={styles.buttons}
                                                                color={colors.APP_BLUE}
                                                                mode="outlined"
                                                                onPress={() => { this.shareHandler(item.titulo, item.descripcion) }}
                                                            >Compartir</Button>
                                                        </Card.Actions>
                                                    </Card>
                                                </TouchableOpacity>
                                            ))
                                        }

                                    </View>
                                </ScrollView>

                                : <View style={styles.empty}>
                                    <Text style={appStyles.textTituloGrande}>No hay iniciativas</Text>
                                </View>
                    }

                </PaperProvider>
            </SpinnerView>

        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        
        backgroundColor: '#f8f8f8'
    },
    card: {
        marginBottom: 10,
        borderWidth: 0.5,
        borderRadius: 4,
        borderColor: Colors.grey400,
    },
    image: {
        //marginRight: 6,
        //marginLeft: 6,
        width: "80%",
        marginBottom: 10,
        borderWidth: 0.5,
        borderRadius: 4,
        zIndex:-2,
    },
    empty: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: '#f8f8f8'
    },
    buttons: {
        borderColor: "#2099FB",
        borderRadius: 10,
        borderWidth: 2,
        width: 120
    },
    // -----  Facu Styling ------
    actionHolder: {
        display: 'flex',
        justifyContent: 'space-around',
    },
    button: {},
    description: {
        margin: 10,
        color: Colors.grey700,
    },
    mainContainer: {
        borderColor: Colors.grey400,
        borderWidth: 1,
        borderRadius: 4,
        marginTop: 10,
        marginBottom: 10,
    },
    image: {},
    progressBar: {
        margin: 10,
    },
    progressContainer: {
        display: 'flex',
        flexDirection: 'column',
    },
    status: {
        marginRight: 15,
        marginBottom: -5,
        color: Colors.grey600,
        alignSelf: 'flex-end',
    },
    statusContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    countdown: {
        marginLeft: 15,
        marginBottom: -5,
        color: Colors.grey600,
    },
    title: {
        marginTop: 15,
        marginBottom: 5,
        marginLeft: 10,
        color: Colors.black,
    },
});

const theme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
        ...DefaultTheme.colors,
        primary: '#2099FB',
        accent: '#47b1a1',
        backgroundColor: '#f8f8f8',
    },
};