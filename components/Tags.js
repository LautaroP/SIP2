import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions
} from 'react-native';

export default class Tags extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [
        {id:1, title: "Problema social", value: "Problema Social", color:"#87CEEB", image:"https://img.icons8.com/plasticine/100/000000/family.png"},
        {id:2, title: "Problema ambiental", value: "Problema Ambiental", color:"#4682B4", image:"https://img.icons8.com/dusk/70/000000/globe-earth.png"} ,
        {id:3, title: "Salud", value: "Salud", color:"#6A5ACD", image:"https://img.icons8.com/plasticine/100/000000/nurse-female.png"} ,
        {id:4, title: "Económico", value: "Economico", color:"#20B2AA", image:"https://img.icons8.com/plasticine/100/000000/cash-.png"} ,
      ],
    };
  }

  habilitarBoton() {
    this.state.disabled === true ?
      this.setState({disabled: false})
      :this.setState({disabled: true});
 }

  render() {
    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={this.state.data}
          horizontal={false}
          numColumns={4}
          keyExtractor= {(item) => {
            return item.id;
          }}
          renderItem={({item}) => {
            return (
              <TouchableOpacity style={[styles.card, {backgroundColor:item.color}]}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image style={styles.cardImage} source={{uri:item.image}}/>
                  <Text style={styles.title}>{item.title}</Text>
                </View>
              </TouchableOpacity>
            )
          }}/>
      </View>
    );
  }
}

var { height, width } = Dimensions.get('window');
var box_count = 4;
var box_width = (width / box_count) * 0.9;
var box_height = height * 0.4

const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  list: {
    backgroundColor:"white",
  },
  listContainer:{
    alignItems:'center'
  },
  /******** card **************/
  card:{
    marginHorizontal:2,
    marginVertical:2,
    flexBasis: box_width,
    height: 35,
    alignItems:'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderWidth: 1,
  },
  cardImage:{
    height: 30,
    width: 30,
    alignSelf:'flex-start',
  },
  title:{
    fontSize:10,
    flex:1,
    color:"#FFFFFF",
    fontWeight:'bold',
    textAlign: 'center',
  },
  icon:{
    height: 20,
    width: 20, 
  }
});   