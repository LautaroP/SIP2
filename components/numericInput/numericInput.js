import React from "react";
import ReinputInput from "reinput";
import { appStyles, colors } from "../../src/index.styles";


const pattern = "  /\(([0-9])\)/";
class FormNumericInput extends React.Component {

    // pattern = (text) => {
    //   return text.replace(text.match('[^0-9]'), "")
    // }
    textInputRef;
    focus() {
        if (this.textInputRef)
            this.textInputRef.focus();
    }
    render() {

        const { style, error, optionalBool, label, ...otherProps } = this.props;
        return (
            <ReinputInput
                ref={(input) => this.textInputRef = input}
                keyboardType={'number-pad'}/*  pattern={this.pattern} */
                fontSize={16}
                style={[style]}
                underlineHeight={0.5}
                underlineActiveHeight={1}
                underlineColor={colors.APP_LIGHT_BLUE}
                labelActiveColor={colors.APP_LIGHT_BLUE}
                underlineActiveColor={error ? 'red' : colors.APP_BLUE}
                labelColor={error ? 'red' : colors.APP_LIGHT_BLUE}
                label={optionalBool ? this.props.label + " (opcional)" : this.props.label}
                {...otherProps}
            />
        );
    }

}

export default FormNumericInput;