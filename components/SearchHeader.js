import React, {Component} from 'react'
import {Provider as PaperProvider, Searchbar} from 'react-native-paper'

class SearchHeader extends Component {
    state = {
        firstQuery: '',
      };
    render () {
        const { firstQuery } = this.state;
        return (
            <Searchbar
                placeholder = "Buscar iniciativa por título"
                onChangeText = { 
                query => { this.setState({ firstQuery: query }); 
                query == '' ?  this.props.action(query) : null }}
                onSubmitEditing = { () => this.props.action(firstQuery) } 
                onIconPress = {() => this.props.action(firstQuery)}
                value = { firstQuery }
                style={{width: '100%'}}
            />                
        )
    }
}

export default SearchHeader;