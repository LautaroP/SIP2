import React from "react";
import { colors } from "../../src/index.styles";
import ReinputInput from "reinput";
import { PixelRatio, Text } from "react-native";

const pattern = "  /\(([0-9])\)/";

export default class EmailInput extends React.Component {

    textInputRef;
    focus() {
        if (this.textInputRef)
            this.textInputRef.focus();
    }
    isFocused() {
        if (this.textInputRef)
            this.textInputRef.register.isFocused();
    }
    onFocus(callback) {
        if (this.textInputRef)
            this.textInputRef.register.onFocus(callback);
    }
    render() {

        const { style, optionalBool, label, ...otherProps } = this.props;

        return (
            <ReinputInput
                autoCapitalize='none'
                ref={(input) => this.textInputRef = input}
                keyboardType={'email-address'}
                fontSize={16}
                underlineHeight={0.5}
                style={{ height: 70 }}
                underlineActiveHeight={1}
                labelActiveColor={colors.TEXTO_PLANO_GREY}
                underlineActiveColor={colors.BLACK}
                {...this.props}
                label={optionalBool ? this.props.label + " (opcional)" : this.props.label}
            />
        );
    }
}