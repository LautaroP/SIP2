import React from "react";
import { appStyles, colors } from "../../src/index.styles";
import ReinputInput from "reinput";

class FormPasswordInput extends React.Component {
  textInputRef;
  isFocused() {
    if (this.textInputRef)
      this.textInputRef.register.isFocused();
  }
  onFocus(callback) {
    if (this.textInputRef)
      this.textInputRef.register.onFocus(callback);
  }
  render() {
    const { style, optionalBool, label, ref, ...otherProps } = this.props;
    return (
      <ReinputInput
        ref={(input) => this.textInputRef = input}
        fontSize={16}
        style={[style]}
        secureTextEntry={true}

        // underlineHeight={0.5}
        // underlineActiveHeight={1}
        underlineColor={colors.APP_LIGHT_BLUE}
        labelActiveColor={colors.APP_LIGHT_BLUE}
        underlineActiveColor={colors.APP_BLUE}
        labelColor={colors.APP_LIGHT_BLUE}
        // register={ref}
        {...this.props}
        label={optionalBool ? this.props.label + " (opcional)" : this.props.label}
      />
    );
  }
}

export default FormPasswordInput;