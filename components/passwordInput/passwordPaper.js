import React from "react";
import { appStyles, colors } from "../../src/index.styles";
import { StyleSheet, Dimensions } from "react-native"
import { TextInput } from "react-native-paper"
//import ReinputInput from "reinput";

class PasswordPaper extends React.Component {



  render() {

    const { style, optionalBool, label, ref, ...otherProps } = this.props;
    return (
      <TextInput
        style={[styles.textInput, style]}
        theme={theme}
        mode="outlined"
        selectionColor="#2099FB"
        underlineColor="#2099FB"
        label={this.props.label} //La idea es que en otherProps se mande lo de label
        onChangeText={this.props.onChangeText}
        value={this.props.value}
        inlineImageLeft={this.props.inlineImageLeft}
        inlineImagePadding={this.props.inlineImagePadding}
        secureTextEntry={true}
      />
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    // margin: 15,
    //height:40,
    //flex: 1,
    width: '100%'
  },
})

const theme = {
  //...DefaultTheme,
  roundness: 10,
  colors: {
    //...DefaultTheme.colors,
    primary: colors.APP_BLUE,
    accent: colors.APP_BLUE_HOVER,
  },
};

export default PasswordPaper;