import React, { Component } from 'react';
import { TouchableHighlight, Text } from 'react-native';
import { appStyles } from '../../src/index.styles';

class LinkButton extends Component {
    state = {
        pressed: false
    }
    render() {
        const { textHoverStyle, textStyle, children, ...otherProps } = this.props
        return (
            <TouchableHighlight activeOpacity={1} underlayColor='transparent' onShowUnderlay={() => this.setState({ pressed: true })}
                onHideUnderlay={() => this.setState({ pressed: false })} {...otherProps}>
                <Text style={this.state.pressed ? textHoverStyle || appStyles.textoHoverGris : textStyle || appStyles.textoGrisLink}>{children}</Text>
            </TouchableHighlight>
        );
    }
}

export default LinkButton;