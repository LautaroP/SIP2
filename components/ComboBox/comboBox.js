import React from 'react';
import { Image, KeyboardAvoidingView, PixelRatio, StyleSheet, Text, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { appStyles, colors } from "../../src/index.styles";


class ComboBox extends React.Component {

    render() {
        const { style, dropdownStyle, optionalBool, label, ...otherProps } = this.props;
        return (
            <View style={[style]}>
                <Dropdown
                    fontSize={16}
                    lineWidth={0.5}
                    itemCount={5}
                    labelTextStyle={{ paddingTop: 3 }}
                    // style={{
                    //     marginBottom: 5,
                    //     marginLeft: 1
                    // }}
                    selectedItemColor={colors.APP_LIGHT_BLUE} //Color de los items del listado
                    textColor={colors.APP_BLUE} // El del placeholder
                    baseColor={colors.APP_BLUE} //La linea de abajo
                    pickerStyle={{backgroundColor: "#F9F9F9"}}
                    label={optionalBool ? this.props.label + " (opcional)" : this.props.label}
                    {...otherProps}
                />

            </View>
        );
    }
}





const styles = StyleSheet.create({

    accessory: {
        width: 20,
        height: '120%', // o 30px en ios
        justifyContent: 'center',
        alignItems: 'center',
    },
    triangle: {
        color: colors.BLACK,
        marginTop: 5
    },
});



export default ComboBox;