import React from 'react';
import App from './index'
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore'
import SpinnerView from './components/SpinnerView';

let store = configureStore;
console.disableYellowBox = true;
const Index = () => (

      <Provider store={store}>
            {/* <SpinnerView> */}
                  <App />
            {/* </SpinnerView> */}
      </Provider>
)

export default Index;